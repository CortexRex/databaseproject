EMAIL_INVALID Your email is invalid
FIRST_NAME_EMPTY Your first name can not be empty or only contain white space
FIRST_NAME_HAS_INVALID_CHARS Your first name contains invalid characters
LAST_NAME_EMPTY Your last name can not be empty or only contain white space
LAST_NAME_HAS_INVALID_CHARS Your last name contains invalid characters
LAST_NAME_TOO_SHORT Your last name is too short
PASSWORD_EMPTY password can not be empty or only contain whitespace
USER_NAME_EMPTY Your user name can not be empty or only contain white space
USER_NAME_DOES_NOT_EXIST your user name does not exist
USER_NAME_HAS_INVALID_CHARS Your user name can only contain letters, numbers, dashes and underscores
PASSWORD_HAS_INVALID_CHARS Your password can only contain letters, numbers, dashes and underscores
PASSWORD_INVALID Password is incorrect
GENDER_EMPTY You must select your gender
GENDER_INVALID Gender entered is invalid
BDAY_EMPTY You must select a day
BDAY_MALFORMED date not input in correct form. yyyy-mm-dd
BDAY_INVALID Not a valid Gregorian date
EMAIL_EMPTY Your email can not be empty or only contain white space
TEL_EMPTY You must enter a telephone number
TEL_INVALID Your telephone number must be valid 10 digit US telephone number
PIC_EMPTY You must enter a filename
INVALID_COLOR The color you entered is invalid
HEARD_EMPTY You must select an option
HEARD_INVALID The option you entered is invalid
USE_EMPTY You must select an option
USE_INVALID The Use you entered is Invalid
GROUP_NAME_EMPTY Your group name can not be empty or contain only white space
GROUP_NAME_INVALID Your group name contains invalid characters
DESCRIPTION_HAS_INVALID_CHARS Your description contains invalid characters
DESCRIPTION_EMPTY Your description can not be empty or contain only white space
USER_INVALID User ID is invalid
USER_COULD_NOT_BE_UPDATED User information could not be updated
CREATOR_NAME_DOES_NOT_EXIST Creator name does not exist in database
GROUP_IDENTITY_INVALID Group ID is invalid
GROUP_DOES_NOT_EXIST Group does not exist
CREATOR_NAME_DOES_NOT_MATCH Group creator names do not match
GROUP_COULD_NOT_BE_UPDATED Group could not be updated
MOVIE_INVALID Movie invalid
MEMBER_INVALID Member is invalid