<?php
class AddMovieController {

	public static function run() {

		$movie = null;
		if ($_SERVER["REQUEST_METHOD"] == "POST"){
			//divy up post into movie, genre, actor, director, and producer arrays
			$movieArray = array("title"=> $_POST['title'], "rentalFee"=> $_POST['rentalFee'],
					"studio"=> $_POST['studio']);
			$movie = new Movie($movieArray);
			$newMovie = movieDB::addMovie($movie);
			//add arrays into DB
			//add copies
			MovieDB::addCopies($newMovie, $_POST['copies']);
			
			//gather producers
			$producers = $_POST['producers'];
			$producers_array = explode(',', $producers);
			MovieDB::addProducers($newMovie, $producers_array);
			//gather directors
			$directors = $_POST['directors'];
			$directors_array = explode(',', $directors);
			MovieDB::addDirectors($newMovie, $directors_array);
			
			//gather actors
			$actors = $_POST['actors'];
			$actors_array = explode(',', $actors);
			MovieDB::addActors($newMovie, $actors_array);
			
			if(is_null($newMovie) || $newMovie->getErrorCount()!= 0){
				AddMovieView::showNew();
			}
		}
		if (is_null($movie) || $movie->getErrorCount() != 0) {
			AddMovieView::showNew();
		} else {
			//we are done, go back to main search
			SearchView::show();
			header('Location: /'.$_SESSION['base'].'/search');
		}
	}
}
?>