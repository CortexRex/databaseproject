<?php
class SignupController {

	public static function run() {
		// Perform actions related to a user
		$action = (array_key_exists('action', $_SESSION))?$_SESSION['action']:"";
		$arguments = $_SESSION['arguments'];
		switch ($action) {
			case "newCustomer":
				self::newCustomer();
				break;
			case "selectCustomer":
				self::searchCustomer();
				break;
			case  "newEmployee":
				self::newEmployee();
				break;
			case "removeEmployee";
			    self::removeEmployee();
			    break;
			case "update":
				echo "Update";
				self::updateUserData();
				break;
			default:
		}
	}
	
	
	public static function searchCustomer() {
		$search = null;
		$value = null;
		$results = null;
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			echo $_POST['searchOption'];
			echo $_POST['searchText'];
			switch ($_POST['searchOption']) {
				case "lName":
					$results = CustomerDB::getCustomersBy('lName', $_POST['searchText']);
					break;
				case "phone":
					$results = CustomerDB::getCustomersBy('phone', $_POST['searchText']);
					break;
				default:
						
			}
			$_SESSION['CResults'] = $results;
			SignupView::showSearchCustomer();
		}
		else{
			SignupView::showSearchCustomer();
		} 
		
	}
	
	public static function newCustomer() {
		// Process a new Customer
		$customer = null;
		if ($_SERVER["REQUEST_METHOD"] == "POST"){
			$customer = new Customer($_POST);
			$_SESSION['customer'] = $customer;
			$newCustomer = CustomerDB::addCustomer($customer); //may need to postpone till payment controller
			if(is_null($newCustomer) || $newCustomer->getErrorCount()!= 0){
				SignupView::showNewCustomer();
			}
		}
		if (is_null($customer) || $customer->getErrorCount() != 0) {
			$_SESSION['customer'] = $customer;
			SignupView::showNewCustomer();
		} else {
			//need to go to payment controller
			
			SearchView::show();
			header('Location: /'.$_SESSION['base'].'/search');
		}
	}
	 
	public static function newEmployee() {
		// Process a new Employee
		$employee = null;
		$role = $_SESSION['authenticatedEmployee']->getRole();
		if ($_SERVER["REQUEST_METHOD"] == "POST"){
			$employee = new Employee($_POST);
			$_SESSION['newEmployee'] = $employee;
			$newEmployee = EmployeeDB::addEmployee($role, $employee);
			if(is_null($newEmployee) || $newEmployee->getErrorCount()!= 0){
				SignupView::showNewEmployee();
			}
		}
		if (is_null($employee) || $employee->getErrorCount() != 0) {
			if(!is_null($employee))
				print_r($employee->getErrors()); //testing
			$_SESSION['newEmployee'] = $employee;
			SignupView::showNewEmployee();
		} else {
			SearchView::show();
			header('Location: /'.$_SESSION['base'].'/search');
		}
	}

	public static function removeEmployee() {
		// Process a new Employee
		$employee = null;
		$role = $_SESSION['authenticatedEmployee']->getRole();
		if ($_SERVER["REQUEST_METHOD"] == "POST"){
			$employee = EmployeeDB::getEmployeesBy('login', $_POST['login']);
			if($role < $employee[0]->getRole()){
				EmployeeDB::removeEmployee($_SESSION['authenticatedEmployee']->getEmployeeID(), $employee[0]->getEmployeeID());
				SearchView::show();
				header('Location: /'.$_SESSION['base'].'/search');
			}
			else{
				//go back to remove employee but with error msg
				SignupView::showRemoveEmployee(1);
			}
		}
		if (is_null($employee)) {
			//go to remove employee view
			SignupView::showRemoveEmployee(0);
		}
	}
	
	 public static function updateUserData() {
	 	// Process updating of user information
	 	$userData = UserDataDB::getUserDatasBy('userId', $_SESSION['arguments']);
	 	if (empty($userData)) {
	 		ProfileView::show();
	 	} elseif ($_SERVER["REQUEST_METHOD"] == "GET") {
	 		$_SESSION['userData'] = $userData[0];;
	 		SignupView::showUpdate();
	 	} else {
	 		$parms = $userData[0]->getParameters();
	 		$parms['userName'] = (array_key_exists('userName', $_POST))?
	 		$_POST['userName']:"";
	 		$parms['password'] = (array_key_exists('password', $_POST))?
	 		$_POST['password']:"";
	 		$parms['firstName'] = (array_key_exists('firstName', $_POST))?
	 		$_POST['firstName']:"";
	 		$parms['lastName'] = (array_key_exists('lastName', $_POST))?
	 		$_POST['lastName']:"";
	 		$parms['gender'] = (array_key_exists('gender', $_POST))?
	 		$_POST['gender']:"";
	 		$parms['bday'] = (array_key_exists('bday', $_POST))?
	 		$_POST['bday']:"";
	 		$parms['email'] = (array_key_exists('email', $_POST))?
	 		$_POST['email']:"";
	 		$parms['tel'] = (array_key_exists('tel', $_POST))?
	 		$_POST['tel']:"";
	 		$parms['pic'] = (array_key_exists('pic', $_POST))?
	 		$_POST['pic']:"";
	 		$parms['heard'] = (array_key_exists('heard', $_POST))?
	 		$_POST['heard']:"";
	 		$parms['siteUse'] = (array_key_exists('siteUse', $_POST))?
	 		$_POST['siteUse']:"";
	 	
	 		$newUserData = new UserData($parms);
	 		$newUserData->setUserId($userData[0]->getUserId());
	 		$userData = UserDataDB::updateUserData($newUserData);
	 
	 		if ($userData->getErrorCount() != 0) {
	 			$_SESSION['userData'] = $userData;
	 			return;
	 			SignupView::showUpdate();
	 		} else {
	 			HomeView::show();
	 			header('Location: /'.$_SESSION['base']);
	 		}
	 	}
	 }
	 
	 
	 
}
?>