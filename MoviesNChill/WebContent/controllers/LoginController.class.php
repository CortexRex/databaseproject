<?php
class LoginController {

	public static function run() {
		$user = null;
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$user = new User($_POST);
			$_SESSION['employee'] = $user;
			$employees = EmployeeDB::getEmployeesBy('login', $user->getUserName());
			if (empty($employees)){
				$user->setError('userName', 'USER_NAME_DOES_NOT_EXIST');
				//echo "EMPLOYEE LOGIN NOT FOUND";//TEST TEST TEST
			}
			else if(($user->getPasswordHash()) == ($employees[0]->getHash())){
				$user = $employees[0];
				//echo "EMPLOYEE IS FOUND"; //TEST TEST TEST
				echo print_r($user->getErrors());
			}
			else{
				$user->setError('password', 'PASSWORD_INVALID');
			}
			//$_SESSION['employee'] = $user;
		
		}
	
		if (is_null($user) || $user->getErrorCount() != 0) 
		  LoginView::show();
		else  {
           $_SESSION['authenticatedEmployee'] = $user;
		   SearchView::show();
		   header('Location: /'.$_SESSION['base'].'/search');
		}
	}
}
?>