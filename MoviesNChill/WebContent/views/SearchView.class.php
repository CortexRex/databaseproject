<?php

class SearchView {
	
  public static function show() {  
  	$_SESSION['headertitle'] = "Movies 'N Chill Search";
  	$_SESSION['styles'] = array('jumbotron.css');
    MasterView::showHeader();
    MasterView::showNavBar();
	SearchView::showDetails();
	MasterView::showPageEnd();
  }
  
  public static function showDetails() {
  	$base = $_SESSION['base'];
  	$results = null;
  	if (array_key_exists ( 'MResults', $_SESSION ) && $_SESSION ['MResults'] != null)
  		$results = $_SESSION ['MResults'];
  	
  	
  	

  	echo '<div class="jumbotron">';
  	echo '<div class="container">';
    
	echo '<br><br>';
	echo '<div class="container-fluid">';
	echo '<div class="row">';
	echo '<div class="col-md-3 col-sm-2 hidden-xs"></div>';
	echo '<div class="col-md-6 col-sm-8 col-xs-12">';
	echo '<h1>MOVIES \'N CHILL</h1>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	echo '</div>';
	
	echo '</header>';
	
	echo '<br><br>';
	echo '<div class="container-fluid">';
	echo '<div class="well col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">';
	echo '<form class="form-inline" action="#" method="post">';
	echo '<div class="input-group col-lg-8 col-md-8 col-sm-8 pull-left col-xs-8">';
    echo '<input id="word" class="form-control" type="text" value="" placeholder="Search" name="searchText" autofocus="autofocus" />';
    echo '<select name = "searchOption" id = "search">';
    echo '<option value = "title">Title</option>';
    echo '<option value = "studio">Studio</option>';
    echo '<option value = "genre">Genre</option>';
    echo '<option value = "actor">Actor</option>';
    echo '<option value = "director">Director</option>';
    echo '<option value = "producer">Producer</option>';
    echo '</select>';
	echo '<div class="input-group-btn">';

	echo '</div>';
	echo '<!-- /btn-group -->';
	echo '</div>';
	echo '<button id="search" data-style="slide-left" class="btn btn-success col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-right" type="submit"> <i id="icon" class="fa fa-search"></i>Search</button>';
	echo '</form>';
	echo '</div>';
	echo '<div id="results" class="well col-lg-10 col-lg-offset-1 col-md-10 col-md-offset-1 col-sm-10 col-sm-offset-1 col-xs-12">';
	echo '<table id="results" class="table table-hover table-striped table-condensed">';
	echo '<thead>';
	echo '<tr>';
	echo '<th></th>';
	echo '<th>Title</th>';
	echo '<th>Rental Fee</th>';
	echo '<th>Studio</th>';
	echo '<th>Producers</th>';
	echo '<th>Directors</th>';
	echo '<th>Actors</th>';
	echo '<th>Genres</th>';
	echo '<th>Copies Available</th>';
	echo '</tr>';
	echo '</thead>';
	echo '<tbody data-link="row" class="rowlink">';
	if($results){
		foreach($results as $movie){
			echo '<tr>';
			echo '<td></td>';
			echo '<td>'.$movie->getTitle().'</td>';
			echo '<td>'.$movie->getRentalFee().'</td>';
			echo '<td>'.$movie->getStudio().'</td>';
			echo '<td>';
					
			$producers = MovieDB::getMovieProducers($movie->getMovieID());
			$string_producers = implode(',', $producers);
			echo $string_producers;		
			echo '</td>';
			echo '<td>';
					
			$directors = MovieDB::getMovieDirectors($movie->getMovieID());
			$string_directors = implode(',', $directors);
			echo $string_directors;

			
			echo '</td>';
			echo '<td>';
					
			$actors = MovieDB::getMovieActors($movie->getMovieID());
			$string_actors = implode(',', $actors);
			echo $string_actors;
			
			echo '</td>';
			echo '<td>';
			//genres
			$genres = MovieDB::getMovieGenres($movie->getMovieID());
			$string_genres = implode(',', $genres);
			echo $string_genres;
				
			echo '</td>';
			echo '<td>'.count(CopyDB::getCopiesBy('movieID', $movie->getMovieID())).'</td>';
			echo '</tr>';
		}
	}

	echo '</tr>';
	echo '</tbody>';
	echo '</table>';
	echo '</div>';
	echo '</div>';


 	//echo '<h3><a href="/'.$base.'/tests.html">Would you like to run the tests?</a></h3>';
    echo '</section>';


    }
  }?>