<?php
class LoginView {
	
	public static function show() {
		
		$_SESSION ['headertitle'] = "Movies 'N Chill Login";
		MasterView::showHeader ();
		MasterView::showNavBar ();
		LoginView::showDetails ();
		//$_SESSION ['footertitle'] = "<h3>The footer goes here</h3>";
		//MasterView::showFooter ();
	}
	
	public static function showDetails() {
		$base = $_SESSION['base'];
		echo '<div class="jumbotron">';
		echo '<div class="container">';
		echo '<br>';
		echo '<h2>Login</h2>';
		echo '</div>';
		echo '</div>';
		//echo '<form method="post" action="login">';
		echo '<form role = "form" enctype="multipart/form-data"
   		   action ="login" method="Post">';
		echo '<p>';
		
		$employee = null;
		if (array_key_exists ( 'employee', $_SESSION ) && $_SESSION ['employee'] != null)
			$employee = $_SESSION ['employee'];
		
		echo '<div class="container-fluid">';
		echo '<div class="row">';
		echo '<div class="col-md-3 col-sm-2 col-xs-1"></div>';
		echo '<div class="col-md-6 col-sm-8 col-xs-10">';
		
		echo 'Username:';
		echo '<input type="text" name="userName"';
		if (! is_null ( $employee )) {
			echo 'value = "' . $employee->getUserName() . '"';
		}
		echo 'required>';
		echo '	<span class="error">';
		if (! is_null ( $employee )) {
			echo $employee->getError ( 'userName' );
		}
		echo '	</span></p> ';
		echo '<br><br>';
		echo 'Password: ';
		echo '<input type="password" name="password" required>';
		echo '<br><br>';
		echo '<input type="submit" name = "submit" value="Submit">';
		echo '</form>';
		echo '</section>';
		//echo '<h3><a href="/'.$base.'/tests.html">Would you like to run the tests?</a></h3>';
	}
}