<?php
class SignupView {
	
	public static function showNewCustomer() {
		// Create a new customer page
		$_SESSION ['headertitle'] = "New customer registration";
		$_SESSION ['styles'] = array ('jumbotron.css');
		MasterView::showHeader ();
		MasterView::showNavbar ();
		self::showNewCustomerDetails ();
		//$_SESSION ['footertitle'] = "<h3>signup footer</h3>";
		//MasterView::showFooter ();
		MasterView::showPageEnd();
	}
	
	public static function showSearchCustomer(){
		// Select a customer page
		$_SESSION ['headertitle'] = "Select Customer";
		$_SESSION ['styles'] = array ('jumbotron.css');
		MasterView::showHeader ();
		MasterView::showNavbar ();
		self::showSearchCustomerDetails ();
		//$_SESSION ['footertitle'] = "<h3>signup footer</h3>";
		//MasterView::showFooter ();
		MasterView::showPageEnd();
		
	}
	
	public static function showNewEmployee() {
		// Create a new employee page
		$_SESSION ['headertitle'] = "New employee registration";
		$_SESSION ['styles'] = array ('jumbotron.css');
		MasterView::showHeader ();
		MasterView::showNavbar ();
		self::showNewEmployeeDetails ();
		//$_SESSION ['footertitle'] = "<h3>signup footer</h3>";
		//MasterView::showFooter ();
		MasterView::showPageEnd();
	}
	
	public static function showRemoveEmployee($error){
		$_SESSION ['headertitle'] = "Deactive Employee";
		$_SESSION ['styles'] = array ('jumbotron.css');
		MasterView::showHeader ();
		MasterView::showNavbar ();
		self::showRemoveEmployeeDetails ($error);
		//$_SESSION ['footertitle'] = "<h3>signup footer</h3>";
		//MasterView::showFooter ();
		MasterView::showPageEnd();
	}
	
	public static function showNewCustomerDetails() {
		echo '<div class="jumbotron">';
		echo '<div class="container">';
		echo '<br>';
		echo '<h2>Create Member Account</h2>';
		echo '</div>';
		echo '</div>';
		$customer = (array_key_exists ( 'customer', $_SESSION )) ? $_SESSION ['customer'] : null;
		$base = (array_key_exists ( 'base', $_SESSION )) ? $_SESSION ['base'] : "";
		echo '<form method="Post" action ="/' . $base . '/signup/newCustomer">';
		echo '<section>';
		echo 'First name: ';
		echo '<input type="text" name="fName"';
		if (! is_null ( $customer ))
			echo 'value = "' . $customer->getFName () . '" required>';
		echo '<span class="error">';
		if (! is_null ( $customer ))
			echo $customer->getError ( 'fName' );
		echo '</span><br><br>';
		echo 'Last name:';
		echo '<input type="text" name="lName"';
		if (! is_null ( $customer ))
			echo 'value = "' . $customer->getLName () . '" required>';
		echo '<span class="error">';
		if (! is_null ( $customer ))
			echo $customer->getError ( 'lName' );
		echo '</span>';
		echo '<br><br>';
		echo 'Telephone:';
		echo '<input type="tel" name="phone"';
		if (! is_null ( $customer ))
			echo 'value = "' . $customer->getPhone () . '">';
		echo '<span class="error">';
		if (! is_null ( $customer ))
			echo $customer->getError ( 'phone' );
		echo '</span>';
		echo '</p>';
		echo '<br><br>';
		echo '<input type="submit" value="Submit">';
		echo '</p>';
		echo '</form>';
		echo '</section>';
	}
	
	public static function showNewEmployeeDetails() {
		echo '<div class="jumbotron">';
		echo '<div class="container">';
		echo '<br>';
		echo '<h2>Create Employee Account</h2>';
		echo '</div>';
		echo '</div>';
		$creatorRole = $_SESSION['authenticatedEmployee'] ->getRole();
		$employee = (array_key_exists ( 'newEmployee', $_SESSION )) ? $_SESSION ['newEmployee'] : null;
		$base = (array_key_exists ( 'base', $_SESSION )) ? $_SESSION ['base'] : "";
		echo '<form method="Post" action ="/' . $base . '/signup/newEmployee">';
		echo '<section>';
		

		echo 'Employee Role: ';
		echo '<select name = "role" id = "role">';
		echo '<option value = 3>Store Employee</option>';
		if($creatorRole == 1)
			echo '<option value = 2>Manager</option>';
		echo '</select>';
		
		echo 'Employee Location: ';
		echo '<select name = "locationID" id = "locationID">';
		echo '<option value = 1>90210 Beverly Hills</option>';
		echo '</select>';
	
		echo '<br><br>';
		echo 'First name: ';
		echo '<input type="text" name="fName"';
		if (! is_null ( $employee ))
			echo 'value = "' . $employee->getFName () . '" required>';
		echo '<span class="error">';
		if (! is_null ( $employee ))
			echo $employee->getError ( 'fName' );
		echo '</span><br><br>';
		echo 'Last name:';
		echo '<input type="text" name="lName"';
		if (! is_null ( $employee ))
			echo 'value = "' . $employee->getLName () . '" required>';
		echo '<span class="error">';
		if (! is_null ( $employee ))
			echo $employee->getError ( 'lName' );
		echo '</span>';
		echo '<br><br>';
		echo 'Telephone:';
		echo '<input type="tel" name="phone"';
		if (! is_null ( $employee ))
			echo 'value = "' . $employee->getPhone () . '">';
		echo '<span class="error">';
		if (! is_null ( $employee ))
			echo $employee->getError ( 'phone' );
		echo '</span>';
		echo '</p>';
		echo '<br><br>';
		echo 'Login:';
		echo '<input type="text" name="login"';
		if (! is_null ( $employee ))
			echo 'value = "' . $employee->getLogin () . '" required>';
		echo '<span class="error">';
		if (! is_null ( $employee ))
			echo $employee->getError ( 'login' );
		echo '</span>';
		echo '<br><br>';
		echo 'Password:';
		echo '<input type="text" name="password"';
		echo '<span class="error">';
		if (! is_null ( $employee ))
			echo $employee->getError ( 'password' );
		echo '</span>';
		echo '<br><br>';
		echo '<input type="submit" value="Submit">';
		echo '</p>';
		echo '</form>';
		echo '</section>';
	}
	
	public static function showRemoveEmployeeDetails($error) {
		echo '<div class="jumbotron">';
		echo '<div class="container">';
		echo '<br>';
		echo '<h2>Remove Employee</h2>';
		echo '</div>';
		echo '</div>';
		$base = (array_key_exists ( 'base', $_SESSION )) ? $_SESSION ['base'] : "";
		echo '<form method="Post" action ="/' . $base . '/signup/removeEmployee">';
		echo '<section>';
		echo 'Employee\'s Login: ';
		echo '<input type="text" name="login"';

		echo '<span class="error">';
		if ($error > 0)
			echo 'Unable to Remove Employee. You do not have authorization';
		echo '</span><br><br>';
		echo '<input type="submit" value="Submit">';
		echo '</p>';
		echo '</form>';
		echo '</section>';
	}
	public static function showSearchCustomerDetails(){
		$results = null;
		$base = $_SESSION['base'];
		if (array_key_exists ( 'CResults', $_SESSION ) && $_SESSION ['CResults'] != null)
			$results = $_SESSION ['CResults'];
		echo '<div class="jumbotron">';
		echo '<div class="container">';
		echo '<br>';
		echo '<h2>Select Member</h2>';
		echo '</div>';
		echo '</div>';
		echo '<div class="container-fluid">';
		echo '<div class="well col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">';
		echo '<form class="form-inline" action="#" method="post">';
		echo '<div class="input-group col-lg-8 col-md-8 col-sm-8 pull-left col-xs-8">';
		echo '<input id="word" class="form-control" type="text" value="" placeholder="Search" name="searchText" autofocus="autofocus" />';
		echo '<select name = "searchOption" id = "search">';
		echo '<option value = "lName">Search by Last Name</option>';
		echo '<option value = "phone">Search by Phone</option>';
		echo '</select>';
		echo '<div class="input-group-btn">';

		echo '</div>';
		echo '<!-- /btn-group -->';
		echo '</div>';
		echo '<button id="search" data-style="slide-left" class="btn btn-success col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-right" type="submit"> <i id="icon" class="fa fa-search"></i>Search</button>';
		echo '</form>';
		echo '</div>';
		echo '<div id="results" class="well col-lg-8 col-lg-offset-2 col-md-8 col-md-offset-2 col-sm-10 col-sm-offset-1 col-xs-12">';
		echo '<table id="results" class="table table-hover table-striped table-condensed">';
		echo '<thead>';
		echo '<tr>';
		echo '<th></th>';
		echo '<th>First Name</th>';
		echo '<th>Last Name</th>';
		echo '<th>Phone</th>';
		echo '</tr>';
		echo '</thead>';
		echo '<tbody data-link="row" class="rowlink">';
		if($results){
			foreach($results as $customer){
				$customerID = $customer->getMemberID();
				echo '<tr>';
				echo '<td></td>';
				echo '<td><a href="/'.$base.'/search/'.$customerID.'">'.$customer->getFName().'</td>';
				echo '<td>'.$customer->getLName().'</td>';
				echo '<td>'.$customer->getPhone().'</td>';
				echo '</tr>';
			}
		}

		echo '</tr>';
		echo '</tbody>';
		echo '</table>';
		echo '</div>';
		echo '</div>';
		
		
	}
	
	public static function showUpdate() {
		$_SESSION ['headertitle'] = "Update user";
		$_SESSION ['styles'] = array ('jumbotron.css');
		MasterView::showHeader ();
		MasterView::showNavbar ();
		self::showUpdateDetails ();
		$_SESSION ['footertitle'] = "The user update footer";
		MasterView::showFooter ();
		MasterView::showPageEnd();
	}
	
	public static function showUpdateDetails() {
		echo '<div class="jumbotron">';
		echo '<div class="container">';
		echo '<br>';
		echo '<h2>Update User Information</h2>';
		echo '</div>';
		echo '</div>';
		
		$userData = (array_key_exists ( 'userData', $_SESSION )) ? $_SESSION ['userData'] : null;
		$base = (array_key_exists ( 'base', $_SESSION )) ? $_SESSION ['base'] : "";
		
		echo '<section>';
		echo '<form method="Post" action ="/' . $base . '/signup/update/' . $userData->getUserId () . '"';
		
		echo '<h3>Personal Information</h3>';
		echo '<p>';
		echo 'First name:';
		echo '<input type="text" name="firstName"';
		if (! is_null ( $userData ))
			echo 'value = "' . $userData->getFirstName () . '" required>';
		echo '<span class="error">';
		if (! is_null ( $userData ))
			echo $userData->getError ( 'firstName' );
		echo '</span><br><br>';
		echo 'Last name:';
		echo '<input type="text" name="lastName"';
		if (! is_null ( $userData ))
			echo 'value = "' . $userData->getLastName () . '" required>';
		echo '<span class="error">';
		if (! is_null ( $userData ))
			echo $userData->getError ( 'lastName' );
		echo '</span>';
		echo '<br><br>';
		echo 'Gender:';
		echo '<input type="radio" name="gender" value="male" checked>Male ';
		echo '<input type="radio" name="gender" value="female">Female';
		echo '<span class="error">';
		if (! is_null ( $userData ))
			echo $userData->getError ( 'gender' );
		echo '</span>';
		echo '<br><br>';
		echo 'Date of Birth (If using firefox or chrome use yyyy-mm-dd):';
		echo '<input type="date" name="bday">';
		echo '<span class="error">';
		if (! is_null ( $userData ))
			echo $userData->getError ( 'bday' );
		echo '</span>';
		echo '<br><br>';
		echo 'Email:';
		echo '<input type="email" name="email"';
		if (! is_null ( $userData ))
			echo 'value = "' . $userData->getEmail () . '" required>';
		echo '<span class="error">';
		if (! is_null ( $userData ))
			echo $userData->getError ( 'email' );
		echo '</span>';
		echo '<br><br>';
		echo 'Telephone:';
		echo '<input type="tel" name="tel"';
		if (! is_null ( $userData ))
			echo 'value = "' . $userData->getTel () . '">';
		echo '<span class="error">';
		if (! is_null ( $userData ))
			echo $userData->getError ( 'tel' );
		echo '</span>';
		echo '</p>';
		echo '<br><br>';
		echo '<h3>Account Information</h3>';
		echo '<p>';
		echo 'Username:';
		echo '<input type="text" name="userName"';
		if (! is_null ( $userData ))
			echo 'value = "' . $userData->getUserName () . '"required>';
		echo '<span class="error">';
		if (! is_null ( $userData ))
			echo $userData->getError ( 'userName' );
		echo '</span>';
		echo '<br><br>';
		echo 'Password:';
		echo '<input type="password" name="password" required>';
		echo '<br><br>';
		echo 'Verify Password:';
		echo '<input type="password" name="passverify" required>';
		echo '</p>';
		echo '<br><br>';
		echo '<h3>Profile Information</h3>';
		echo '<p>';
		echo 'Profile Picture:';
		echo '<input type="file" name="pic">';
		echo '<span class="error">';
		if (! is_null ( $userData ))
			echo $userData->getError ( 'pic' );
		echo '</span>';
		echo '<br><br>';
		echo '<h3>Other Information</h3>';
		echo '<p>';
		echo 'How did you hear about us?<br>';
		echo '<input type="checkbox" name="heard" value="friend">From a Friend';
		echo '<br>';
		echo '<input type="checkbox" name="heard" value="social">Social Network';
		echo '<br>';
		echo '<input type="checkbox" name="heard" value="search">Search Engine';
		echo '<br>';
		echo '<input type="checkbox" name="heard" value="other">Other';
		echo '	<span class="error">';
		if (! is_null ( $userData ))
			echo $userData->getError ( 'heard' );
		echo '</span>';
		echo '<br><br>';
		echo 'How will you be using this site?';
		echo '<select name="siteUse">';
		echo '<option value="project" selected>Group Project</option>';
		echo '<option value="groupwork">Ongoing Group Work</option>';
		echo '<option value="faculty">Teacher Overseeing Students</option>';
		echo '</select>';
		echo '<span class="error">';
		if (! is_null ( $userData ))
			echo $userData->getError ( 'siteUse' );
		echo '</span>';
		echo '<br><br>';
		echo '<input type="submit" value="Submit">';
		echo '</p>';
		echo '</form>';
		echo '</section>';
	}
}
?>