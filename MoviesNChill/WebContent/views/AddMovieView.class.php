<?php
class AddMovieView {
	public static function showNew() {
		// Create a new movie page
		$_SESSION ['headertitle'] = "New movie";
		$_SESSION ['styles'] = array (
				'jumbotron.css' 
		);
		MasterView::showHeader ();
		MasterView::showNavbar ();
		self::showNewDetails ();
		//$_SESSION ['footertitle'] = "<h3>signup footer</h3>";
		//MasterView::showFooter ();
		MasterView::showPageEnd ();
	}
	public static function showNewDetails() {
		echo '<div class="jumbotron">';
		echo '<div class="container">';
		echo '<br>';
		echo '<h2>Add New Movie</h2>';
		echo '</div>';
		echo '</div>';
		$base = (array_key_exists ( 'base', $_SESSION )) ? $_SESSION ['base'] : "";
		echo '<form method="Post" action ="/' . $base . '/addMovie">';
		echo '<section>';
		
		echo 'Title: ';
		echo '<input type="text" name="title"';
		echo '<span class="error">';
		// no errors currently
		echo '</span><br><br>';
		
		echo 'Rental Fee:';
		echo '<select name = "rentalFee" id = "rentalFee">';
		echo '<option value = 3.00> 3.00 </option>';
		echo '<option value = 4.00> 4.00 </option>';
		echo '<option value = 4.00> 5.00 </option>';
		echo '<option value = 4.00> 6.00 </option>';
		echo '<option value = 4.00> 7.00 </option>';
		echo '</select>';
		echo '<span class="error">';
		// no errors currently
		echo '</span>';
		echo '<br><br>';
		
		echo 'Studio: ';
		echo '<input type="text" name="studio">';
		echo '<span class="error">';
		// no errors currently
		echo '</span><br><br>';
		
		
		echo 'Copies: ';
		echo '<input type="number" name="copies" min="0" max="100"><br><br>';
	    //no errors currently
	    
		echo 'Producers (list seperated by commas): ';
		echo '<input type="text" name="producers"><br>';

		echo 'Directors (list seperated by commas): ';
		echo '<input type="text" name="directors"><br>';
		
		echo 'Actorss (list seperated by commas): ';
		echo '<input type="text" name="actors"><br>';
		
		echo '</p>';
		echo '<br><br>';
		echo '<input type="submit" value="Submit">';
		echo '</p>';
		echo '</form>';
		echo '</section>';
	}
}
?>
