<?php
class MasterView {
	public static function showHeader() {
        echo '<!DOCTYPE html lang="en"><html><head>';
        echo '<meta charset="utf-8">';
        echo '<meta http-equiv="X-UA-Compatible" content="IE=edge">';
        echo '<meta name="viewport" content="width=device-width, initial-scale=1">';
        echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">';
        echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">';
        echo '<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/css/jasny-bootstrap.min.css">';
        echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>';
        echo '<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>';
        echo '<script src="//cdnjs.cloudflare.com/ajax/libs/jasny-bootstrap/3.1.3/js/jasny-bootstrap.min.js"></script>';
        $styles = (array_key_exists('styles', $_SESSION))? $_SESSION['styles']: array();
        $base = (array_key_exists('base', $_SESSION))? $_SESSION['base']: "";
        foreach ($styles as $style ) 
           echo '<link href="/'.$base.'/css/'.$style. '" rel="stylesheet">';
        $title = (array_key_exists('headertitle', $_SESSION))? $_SESSION['headertitle']: "";
        echo "<title>$title</title>";
        echo "</head><body>";
    }
    
    public static function showNavBar() {
    	// Show the navbar
    	$base = (array_key_exists('base', $_SESSION))? $_SESSION['base']: "";
    	$authenticatedEmployee = (array_key_exists('authenticatedEmployee', $_SESSION))? 
    	                 $_SESSION['authenticatedEmployee']:null;
    	$employee = (array_key_exists('employee', $_SESSION))?$_SESSION['employee']:null;
    	echo '<nav class="navbar navbar-inverse navbar-fixed-top">';
    	echo '<div class="container-fluid">';
    	echo '<div class="navbar-header">';
    	echo '<button type="button" class="navbar-toggle collapsed"';
    	echo 'data-toggle="collapse" data-target="#navbar"';
    	echo 'aria-expanded="false" aria-controls="navbar">';
    	echo '<span class="icon-bar"></span>';
    	echo '<span class="icon-bar"></span>';
    	echo '<span class="icon-bar"></span>';
    	echo '</button>';
    	echo '<a class="navbar-brand" href="/'.$base.'/logout">Movies \'N Chill</a>';
    	echo '</div>';
    	echo '<div id="navbar" class="navbar-collapse collapse">';
    	echo '<ul class="nav navbar-nav">';
    	//If Logged In
    	if (!is_null($authenticatedEmployee)){
    	   echo '<li class="active"><a href="/'.$base.'/signup/newCustomer/">New Customer</a></li>';
    	   echo '<li class="active"><a href="/'.$base.'/signup/selectCustomer">Select Customer</a></li>';
    	   $role = $authenticatedEmployee->getRole();
    	   if($role == 2 || $role == 1){
    	      echo '<li class="active"><a href="/'.$base.'/signup/newEmployee">New Employee</a></li>';
    	      echo '<li class="active"><a href="/'.$base.'/signup/removeEmployee">Remove Employee</a></li>';
    	      echo '<li class="active"><a href="/'.$base.'/addMovie/">Add Movie</a></li>';
    	   }
    	}
    	echo '</ul>';
	
    	if (!is_null($authenticatedEmployee)) {
    		echo '<form class="navbar-form navbar-right"
    			    method="post" action="/'.$base.'/logout">';
    		echo '<div class="form-group">'; //TESTING PLACEMENT
    		/*if (array_key_exists ( 'customer', $_SESSION ) && $_SESSION ['customer'] != null){
    			echo '<span class="label label-default">Selected User: '.
    					$_SESSION['customer']->getFName().' '.$_SESSION['customer']->getLName().'</span>&nbsp; &nbsp;';
    		}*/
    		echo '<span class="label label-default">
                  Hi '.$authenticatedEmployee->getLogin();
    		echo '     ';
                  		
            if (array_key_exists ( 'customer', $_SESSION ) && $_SESSION ['customer'] != null){
    	    	echo 'Selected User: '.$_SESSION['customer']->getFName().' '.$_SESSION['customer']->getLName();
            }
    		
   
            echo'</span>&nbsp; &nbsp;';
    		echo '</div>';
    		echo '<button type="submit" class="btn btn-success">Sign out</button>';
    		echo '</form>';
    	} else {
	    	echo '<form class="navbar-form navbar-right" 
	    			    method="post" action="/'.$base.'/login">';
	    	echo '<div class="form-group">';
	    	echo '<input type="text" placeholder="User name" class="form-control" name ="userName" ';
	        if (!is_null($employee)) 
	   	        echo 'value = "'. $employee->getUserName().'"';
	    	echo 'required></div>';
	    	echo '<div class="form-group">';
	    	echo '<input type="password" placeholder="Password" 
	    			      class="form-control" name ="password">';
	    	echo '</div>';
	    	echo '<button type="submit" class="btn btn-success">Sign in</button>';
	    	//echo '<a class="btn btn-primary" href="/'.$base.'/signup/new" role="button">Register</a></p>';
	    	echo '</form>';

    	}
    	echo '</div>';
    	echo '</div>';
    	echo '</nav>';
    }
    

	public static function showFooter() {
		$footer = (array_key_exists('footertitle', $_SESSION))?
		           $_SESSION['footertitle']: "";
		echo '<footer>'.$footer.'</footer>';	
    }
		           		
	public static function showPageEnd() { 
       echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>';
       echo '<script src="../../dist/js/bootstrap.min.js"></script>';
       echo '<script src="../../assets/js/ie10-viewport-bug-workaround.js"></script>';
       echo '</body></html>';
    }
}
?>