<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Basic tests for Customer</title>
</head>
<body>
<h1>Customer tests</h1>

<?php
include_once("../models/Customer.class.php");
?>

<h2>It should create a valid Customer object when all input is provided</h2>
<?php 
$validTest = array("fName"=>"John", "lName"=> "Smith", 
		            "phone"=> "2104325432");
$s1 = new Customer($validTest);
echo "The object is: $s1<br>";
$test1 = (is_object($s1))?'':
'Failed:It should create a valid object when valid input is provided<br>';
echo $test1;
$test2 = (empty($s1->getErrors()))?'':
'Failed:It not have errors when valid input is provided<br>'
.print_r($s1->getErrors());
echo $test2;
?>

<h2>It should extract the parameters that went in</h2>
<?php 
$props = $s1->getParameters();
print_r($props);
?>

<h2>It should have an error when the first name contains invalid characters</h2>
<?php 
$invalidTest = $validTest;
$invalidTest['fName'] = "john$";
$s1 = new Customer($invalidTest);
$test1 = (!empty($s1->getErrors()))?'':
'Failed:It should have errors when invalid input is provided<br>';
echo $test1;
echo "The error for firstName is: ". $s1->getError('fName') ."<br>";
echo "The object is: <br> $s1<br>";
?>

<h2> It should have an error when first name is empty</h2>
<?php 
$invalidTest = $validTest;
$invalidTest['fName'] = "";
$s1 = new Customer($invalidTest);
$test1 = (!empty($s1->getErrors()))?'':
'Failed:It should have errors when invalid input is provided<br>';
echo $test1;
echo "The error for fName is: ". $s1->getError('fName') ."<br>";
echo "The object is:  <br> $s1<br>";
?>

<h2> It should have an error when last name contains invalid characters</h2>
<?php 
$invalidTest = $validTest;
$invalidTest['lName'] = "smith$";
$s1 = new Customer($invalidTest);
$test1 = (!empty($s1->getErrors()))?'':
'Failed:It should have errors when invalid input is provided<br>';
echo $test1;
echo "The error for lName is: ". $s1->getError('lName') ."<br>";
echo "The object is:  <br> $s1<br>";
?>

<h2> It should have an error when last name is empty</h2>
<?php 
$invalidTest = $validTest;
$invalidTest['lName'] = "";
$s1 = new Customer($invalidTest);
$test1 = (!empty($s1->getErrors()))?'':
'Failed:It should have errors when invalid input is provided<br>';
echo $test1;
echo "The error for lName is: ". $s1->getError('lName') ."<br>";
echo "The object is:  <br> $s1<br>";
?>

</body>
</html>