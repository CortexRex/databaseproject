<?php

class DBMaker {
	public static function create($dbName) {
		// Creates a database named $dbName for testing and returns connection
		$db = null;
		try {
			$dbspec = 'mysql:host=localhost;dbname=' . "". ";charset=utf8";
			$username = 'root';
			$password = '';
			$options = array (
					PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION 
			);
			$db = new PDO ( $dbspec, $username, $password, $options );
			$st = $db->prepare ( "DROP DATABASE if EXISTS $dbName" );
			$st->execute ();
			$st = $db->prepare ( "CREATE DATABASE $dbName" );
			$st->execute ();
			$st = $db->prepare ( "USE $dbName" );
			$st->execute ();
			
			
			$st = $db->prepare ( "DROP TABLE if EXISTS Movies" );
			$st->execute ();
			$st = $db->prepare ( "CREATE TABLE Movies (  
					movieID int NOT NULL AUTO_INCREMENT,
   					title varchar(50) NOT NULL,
  					rentalFee decimal(10,2) NOT NULL,
  					studio varchar(50) NOT NULL,
   					PRIMARY KEY (movieID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
			$st->execute ();
						
			$st = $db->prepare ("DROP TABLE if EXISTS Copy");
			$st->execute ();
			
			$st = $db->prepare ("CREATE TABLE Copy (
 				 copyID int NOT NULL AUTO_INCREMENT,
 				 movieID int NOT NULL,
  				 FOREIGN KEY (movieID)
    			 REFERENCES Movie(movieID)
     			 ON DELETE CASCADE, 
  				 PRIMARY KEY (copyID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci");
			$st->execute ();
	/*		
			$st = $db->prepare ("DROP TABLE if EXISTS M_Genre");
			$st->execute ();
			
			$st = $db->prepare ("CREATE TABLE M_Genre (
  				 movieID int NOT NULL,
  				 genre varchar(50) NOT NULL,
  				 FOREIGN KEY (movieID)
     			 REFERENCES Movie(movieID)
      			ON DELETE CASCADE)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();
			
			$st = $db->prepare ("DROP TABLE if EXISTS M_Actor");
			$st->execute ();
				
			$st = $db->prepare ("CREATE TABLE M_Actor (
  				 movieID int NOT NULL,
   				actor varchar(50) NOT NULL,
   				FOREIGN KEY (movieID)
     			 REFERENCES Movie(movieID)
      			ON DELETE CASCADE)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();
			
			$st = $db->prepare ("DROP TABLE if EXISTS M_Director");
			$st->execute ();
			
			$st = $db->prepare ("CREATE TABLE M_Director (
   				movieID int NOT NULL,
   				director varchar(50) NOT NULL,
   				FOREIGN KEY (movieID)
     			 REFERENCES Movie(movieID)
    			 ON DELETE CASCADE)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();			
			
			$st = $db->prepare ("DROP TABLE if EXISTS M_Producer");
			$st->execute ();
				
			$st = $db->prepare ("CREATE TABLE M_Producer (
  				movieID int NOT NULL,
   				producer varchar(50) NOT NULL,
   				FOREIGN KEY (movieID)
     			REFERENCES Movie(movieID)
      			ON DELETE CASCADE)ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();	
			
			$st = $db->prepare ("DROP TABLE if EXISTS Location");
			$st->execute ();
			
			$st = $db->prepare ("CREATE TABLE Location (
   				locationID int NOT NULL AUTO_INCREMENT,
  				address varchar(50) NOT NULL,
   				phone char(10) NOT NULL,
  				PRIMARY KEY (locationID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();	
			
			$st = $db->prepare ("DROP TABLE if EXISTS Roles");
			$st->execute ();
				
			$st = $db->prepare ("CREATE TABLE Roles (
   				role tinyint NOT NULL AUTO_INCREMENT,
   				title varchar(50) NOT NULL,
   				PRIMARY KEY (role, title))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();			
			
			$st = $db->prepare ("DROP TABLE if EXISTS Employee");
			$st->execute ();
			
			$st = $db->prepare ("CREATE TABLE Employee (
   				employeeID int NOT NULL AUTO_INCREMENT,
   				role tinyint NOT NULL, # Used to define a user's role.
   				login varchar(50) NOT NULL,
   				hash char(128) NOT NULL, # Meant to hold SHA-512 hash.
   				fName varchar(50) NOT NULL,
   				lName varchar(50) NOT NULL,
   				hireDate date NOT NULL,
   				phone char(10) NULL,
   				FOREIGN KEY (role)
      			REFERENCES Roles(role),
   				PRIMARY KEY (employeeID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();
			
			$st = $db->prepare ("DROP TABLE if EXISTS StoreEmployee");
			$st->execute ();
				
			$st = $db->prepare ("CREATE TABLE StoreEmployee (
   				employeeID int NOT NULL,
   				locationID int NOT NULL,
   				FOREIGN KEY (employeeID)
      			REFERENCES Employee(employeeID)
     			ON DELETE CASCADE,
   				FOREIGN KEY (locationID)
      			REFERENCES Location(locationID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();			
			
			$st = $db->prepare ("DROP TABLE if EXISTS Customer");
			$st->execute ();
			
			$st = $db->prepare ("CREATE TABLE Customer (
  				memberID int NOT NULL AUTO_INCREMENT,
   				fName varchar(50) NOT NULL,
   				lName varchar(50) NULL,
   				phone char(10) NULL,
   				PRIMARY KEY (memberID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();			
			
			$st = $db->prepare ("DROP TABLE if EXISTS Payment");
			$st->execute ();
				
			$st = $db->prepare ("CREATE TABLE Payment (
   				paymentID int NOT NULL AUTO_INCREMENT,
   				memberID int NOT NULL,
   				fullName varchar(50) NOT NULL,
   				cardType varchar(50) NOT NULL,
   				cardNum char(16) NOT NULL,
   				cardExDate date NOT NULL, # Ignore day for date.
   				FOREIGN KEY (memberID)
      			REFERENCES Customer(memberID),
   				PRIMARY KEY (paymentID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();			
			
			$st = $db->prepare ("DROP TABLE if EXISTS Orders");
			$st->execute ();
			
			$st = $db->prepare ("CREATE TABLE Orders (
   				copyID int NOT NULL,
   				employeeID int NOT NULL,
   				FOREIGN KEY (copyID)
      			REFERENCES Copy(copyID),
   				FOREIGN KEY (employeeID)
      			REFERENCES Employee(employeeID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();			
			
			$st = $db->prepare ("DROP TABLE if EXISTS Rents");
			$st->execute ();
				
			$st = $db->prepare ("CREATE TABLE Rents (
   				rentID int NOT NULL AUTO_INCREMENT,
   				copyID int NOT NULL,
   				memberID int NOT NULL,
   				employeeID int NOT NULL,
   				paymentID int NOT NULL,
   				cost decimal(10,2) NULL,
   				fee decimal(10,2) DEFAULT 0.00,
   				rentDate date NULL,
   				returnDate date NULL,
   				returned bool NOT NULL,
   				FOREIGN KEY (copyID)
      			REFERENCES Copy(copyID),
   				FOREIGN KEY (memberID)
      			REFERENCES Customer(memberID),
   				FOREIGN KEY (employeeID)
      			REFERENCES Employee(employeeID),
   				FOREIGN KEY (paymentID)
      			REFERENCES Payment(paymentID),
   				PRIMARY KEY (rentID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();			
			
			$st = $db->prepare ("DROP TABLE if EXISTS EventLog");
			$st->execute ();
			
			$st = $db->prepare ("CREATE TABLE EventLog (
   				logID int NOT NULL AUTO_INCREMENT,
   				loggedAt datetime NOT NULL,
   				descr varchar(256) NOT NULL,
   				PRIMARY KEY (logID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();			
			
			$st = $db->prepare ("DROP TABLE if EXISTS EmployeeLog");
			$st->execute ();
				
			$st = $db->prepare ("CREATE TABLE EmployeeLog (
   				logID int NOT NULL,
   				adminID int NOT NULL,
   				employeeID int NOT NULL,
   				FOREIGN KEY (logID)
      			REFERENCES EventLog(logID),
   				FOREIGN KEY (adminID)
      			REFERENCES Employee(employeeID),
   				FOREIGN KEY (employeeID)
      			REFERENCES Employee(employeeID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();	
			
			$st = $db->prepare ("DROP TABLE if EXISTS CustomerLog");
			$st->execute ();
			
			$st = $db->prepare ("CREATE TABLE CustomerLog (
   				logID int NOT NULL,
   				employeeID int NOT NULL,
   				memberID int not NULL,
   				FOREIGN KEY (logID)
      			REFERENCES EventLog(logID),
   				FOREIGN KEY (employeeID)
      			REFERENCES Employee(employeeID),
   				FOREIGN KEY (memberID)
      			REFERENCES Customer(memberID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();
			
			$st = $db->prepare ("DROP TABLE if EXISTS TransitionLog");
			$st->execute ();
				
			$st = $db->prepare ("CREATE TABLE TransitionLog (
  		 		logID int NOT NULL,
   				employeeID int NOT NULL,
   				paymentID int NOT NULL,
   				amount decimal(10,2) NOT NULL,
   				FOREIGN KEY (logID)
      			REFERENCES EventLog(logID),
   				FOREIGN KEY (employeeID)
      			REFERENCES Employee(employeeID),
   				FOREIGN KEY (paymentID)
      			REFERENCES Payment(paymentID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();
			
			$st = $db->prepare ("DROP TABLE if EXISTS TransitionLog");
			$st->execute ();
			
			$st = $db->prepare ("CREATE TABLE TransitionLog (
  		 		logID int NOT NULL,
   				employeeID int NOT NULL,
   				paymentID int NOT NULL,
   				amount decimal(10,2) NOT NULL,
   				FOREIGN KEY (logID)
      			REFERENCES EventLog(logID),
   				FOREIGN KEY (employeeID)
      			REFERENCES Employee(employeeID),
   				FOREIGN KEY (paymentID)
      			REFERENCES Payment(paymentID))ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci"
			);
			$st->execute ();
			
			//NO PROCEDURES YET
		*/		
			
		/*	
		 INSERT INTO Employee
(
   employeeID, role, login, hash,
   fName, lName, hireDate, phone
)
VALUES
(
   NULL, (SELECT role FROM Roles WHERE title='Admin'),
   'admin', (SELECT SHA2('admin', 512)), 'George', 'Washington', CURDATE(), '2105555555'
);
*/
			$sql = "INSERT INTO Employee (employeeID, role, login, hash, fName, lName, hireDate, phone) 
					VALUES (NULL,(SELECT role FROM Roles WHERE title='Admin') , 'admin', (SELECT SHA2('admin', 512)), 'George', 'Washington',
					       CURDATE(), '2105555555')";
			$st = $db->prepare ( $sql );
			$st->execute ();
	
			
			
			
			/*
			$st->execute (array (':userId' => 2, ':userName' => 'Jsmith', ':password' => 'yyy', ':firstName' => 'John',
					':lastName' => 'Smith', ':gender' => 'male', ':bday' => '1975-11-19', ':email' => 'jsmith@hotmail.com',
					':tel' => '2104723444', ':pic' => 'pic2.jpg', ':heard' => 'friend', ':siteUse' => 'project'));
			
			$st->execute (array (':userId' => 3, ':userName' => 'AliceWonder', ':password' => 'zzz', ':firstName' => 'Alice',
					':lastName' => 'Jeffreys', ':gender' => 'female', ':bday' => '1981-05-11', ':email' => 'rabbitalice@hotmail.com',
					':tel' => '2101234567', ':pic' => 'pic3.jpg', ':heard' => 'friend', ':siteUse' => 'project'));
			
			
			$sql = "INSERT INTO Groups (groupId, creatorId, groupName, description)
	                             VALUES (:groupId, :creatorId, :groupName, :description)";
			$st = $db->prepare ( $sql );
			$st->execute (array (':groupId' => 1, ':creatorId' => 1, ':groupName' => 'AwesomeGroup',
					             ':description' => 'This Group is Awesome'));
			
			$st->execute (array (':groupId' => 2, ':creatorId' => 1, ':groupName' => 'Nature R Us',
					             ':description' => 'Group for Bio Project'));
			
			$st->execute (array (':groupId' => 3, ':creatorId' => 3, ':groupName' => 'Cs Web Tech',
				                 ':description' => 'This group is for web tech project'));
		
			$sql = "INSERT INTO Members (memberId, userId, groupId)
					 VALUES (:memberId, :userId, :groupId)";
			$st = $db->prepare( $sql);
			$st->execute(array (':memberId' => 1, ':userId' => 1, ':groupId' => 1));
			$st->execute(array (':memberId' => 2, ':userId' => 1, ':groupId' => 2));
			$st->execute(array (':memberId' => 3, ':userId' => 2, ':groupId' => 3));
*/
				
		} catch ( PDOException $e ) {
			echo $e->getMessage (); // not final error handling
		}
		
		return $db;
	}
	public static function delete($dbName) {
		// Delete a database named $dbName
		try {
			$dbspec = 'mysql:host=localhost;dbname=' . $dbName . ";charset=utf8";
			$username = 'root';
			$password = '';
			$options = array (PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
			$db = new PDO ($dbspec, $username, $password, $options);
			$st = $db->prepare ("DROP DATABASE if EXISTS $dbName");
			$st->execute ();
		} catch ( PDOException $e ) {
			echo $e->getMessage (); // not final error handling
		}
	}
}
?>