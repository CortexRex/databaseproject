<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Basic tests for UsersDB</title>
</head>
<body>
<h1>EmployeeDB tests</h1>


<?php
include_once("../models/Database.class.php");
include_once("../models/Employee.class.php");
include_once("../models/EmployeeDB.class.php");
//include_once("./DBMaker.class.php");
?>


<h2>It should get all users from a test database</h2>
<?php
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$employees= EmployeeDB::getEmployeesBy();
$employeeCount = count($employees);
echo "Number of employees in db is: $employeeCount <br>";
foreach ($employees as $employee) 
	echo "$employee <br><br>";
?>	

<h2>It should allow a user to be added</h2>
<?php  
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
echo "Number of users in db before added is: ". count(EmployeeDB::getEmployeesBy()) ."<br>";
$validTest = array("fName"=>"John", "lName"=> "Smith", 
		           "role"=> 1, "hireDate" => "1985-10-31", 
		            "phone"=> "2104325432",
		            "login" => "jsmith85", "password" => "password", 
		            "locationID" => 1);
$employee = new Employee($validTest);
$newEmployee = EmployeeDB::addEmployee(1,$employee);
echo "Number of users in db after added is: ". count(EmployeeDB::getEmployeesBy()) ."<br>";
echo "The new user is: $newEmployee<br>";
?>

<h2>It should get an Employee by login</h2>
<?php 
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$employee = EmployeeDB::getEmployeesBy('login', 'jsmith85');
echo "The value of Employee jsmith85 is:<br>$employee[0]<br>";
?>

<h2>It should allow a user to be removed</h2>
<?php  
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
echo "Number of users in db before removed is: ". count(EmployeeDB::getEmployeesBy()) ."<br>";
$employeeID = $employee[0]->getEmployeeID();
if(!EmployeeDB::RemoveEmployee(1,3)){
	echo "Remove Failed";
}
echo "Number of users in db after removed is: ". count(EmployeeDB::getEmployeesBy()) ."<br>";
?>

<h2>It should not add an invalid user</h2>
<?php 
DBMaker::create('ptest');
Database::clearDB();
$db = Database::getDB('ptest');
echo "Number of users in db before added is: ". count(UserDataDB::getUserDatasBy()) ."<br>";
$invalidUser = array("userName" => "joan&&&#@", "password" => "123", "firstName" => "Joanne",
		           "lastName" => "ofArc", "gender" => "female", "bday" => "1991-10-10",
		           "email" => "jArc@yahoo.com", "tel" => "2108675309", "pic" => "wow.jpg",
		           "heard" => "friend", "siteUse" => "project");
$userDataInvalid = new UserData($invalidUser);
$newUser = UserDataDB::addUserData($userDataInvalid);
echo "Number of users in db after added is: ". count(UserDataDB::getUserDatasBy()) ."<br>";
echo "New user is: $newUser<br>";
?>

<h2>It should not add a duplicate user</h2>
<?php  
DBMaker::create('ptest');
Database::clearDB();
$db = Database::getDB('ptest');
echo "Number of users in db before added is: ". count(UserDataDB::getUserDatasBy()) ."<br>";
$duplicateUser = array("userName" => "dmd", "password" => "xxx", "firstName" => "Derek",
		"lastName" => "Deshaies", "gender" => "male", "bday" => "1985-10-31",
		"email" => "dmd@hotmail.com", "tel" => "2104713421", "pic" => "pic.jpg",
		"heard" => "friend", "siteUse" => "project");
$userDataDuplicate = new UserData($duplicateUser);
$newUser = UserDataDB::addUserData($userDataDuplicate);
echo "Number of users in db after added is: ". count(UserDataDB::getUserDatasBy()) ."<br>";
echo "New user is $newUser <br>";
?>

<h2>It should get a User by userName</h2>
<?php 
DBMaker::create('ptest');
Database::clearDB();
$db = Database::getDB('ptest');
$users = UserDataDB::getUserDatasBy('userName', 'dmd');
echo "The value of User dmd is:<br>$users[0]<br>";
?>

<h2>It should get a User by userId</h2>
<?php 
DBMaker::create('ptest');
Database::clearDB();
$db = Database::getDB('ptest');
$users = UserDataDB::getUserDatasBy('userId', '3');
echo "The value of User 3 is:<br>$users[0]<br>";
?>

<h2>It should not get a User not in Users</h2>
<?php  
DBMaker::create('ptest');
Database::clearDB();
$db = Database::getDB('ptest');
$users = UserDataDB::getUserDatasBy('userName', 'Alfred');
if (empty($users))
	echo "No User Alfred";
else echo "The value of User Alfred is:<br>$users[0]<br>";
?>

<h2>It should not get a User by a field that isn't there</h2>
<?php  
DBMaker::create('ptest');
Database::clearDB();
$db = Database::getDB('ptest');
$users = UserDataDB::getUserDatasBy('social_security', '21052348234');
if (empty($users))
	echo "No User with this social security number";
else 
	echo "The value of User with a specified social security number is:<br>$user<br>";
?>

<h2>It should get a user name by user id</h2>
<?php 
DBMaker::create('ptest');
Database::clearDB();
$db = Database::getDB('ptest');
$userNames = UserDataDB::getUserDataValuesBy('userName', 'userId', 1);
print_r($userNames);
?>

<h2>It should allow update of the user name</h2>
<?php 
DBMaker::create('ptest');
Database::clearDB();
$db = Database::getDB('ptest');
$users = UserDataDB::getUserDatasBy('userId', 1);
$user = $users[0];
echo "<br>Before update: $user <br>";
$parms = $user->getParameters();
$parms['userName'] = 'DRex';
$newUser = new UserData($parms);
$newUser->setUserId(1);
$user = UserDataDB::updateUserData($newUser);
echo "<br>After update: $user <br>";

?>
</body>
</html>