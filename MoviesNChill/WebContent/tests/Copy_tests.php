
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Basic tests for Copy</title>
</head>
<body>
<h1>Copy tests</h1>

<?php
include_once("../models/Copy.class.php");
?>

<h2>It should create a valid Copy object when all input is provided</h2>
<?php 
$validTest = array("movieID"=> 2);
$s1 = new Copy($validTest);
echo "The object is: $s1<br>";
$test1 = (is_object($s1))?'':
'Failed:It should create a valid object when valid input is provided<br>';
echo $test1;
$test2 = (empty($s1->getErrors()))?'':
'Failed:It not have errors when valid input is provided<br>'
.print_r($s1->getErrors());
echo $test2;
?>

<h2>It should extract the parameters that went in</h2>
<?php 
$props = $s1->getParameters();
print_r($props);
?>

</body>
</html>