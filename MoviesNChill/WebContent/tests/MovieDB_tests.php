<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Basic tests for MovieDB</title>
</head>
<body>
<h1>MovieDB tests</h1>


<?php
include_once("../models/Database.class.php");
include_once("../models/Movie.class.php");
include_once("../models/MovieDB.class.php");
//include_once("./DBMaker.class.php");
?>


<h2>It should get all movies from a test database</h2>
<?php
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$movies= MovieDB::getMoviesBy();
$movieCount = count($movies);
echo "Number of movies in db is: $movieCount <br>";
foreach ($movies as $movie) 
	echo "$movie <br><br>";
?>	

<h2>It should allow a movie to be added</h2>
<?php  
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
echo "Number of movies in db before added is: ". count(MovieDB::getMoviesBy()) ."<br>";
$validTest = array("title"=>"Mars Attacks", "rentalFee"=> 6.50, 
		           "studio"=> "Universal Studios");
$movie = new Movie($validTest);
$newMovie = movieDB::addMovie($movie);
echo "Number of movies in db after added is: ". count(MovieDB::getMoviesBy()) ."<br>";
echo "The new movie is: $newMovie<br>";
?>

<h2>It should get a movie by title</h2>
<?php 
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$movie = MovieDB::getMoviesBy('title', 'Mars Attacks');
echo "The value of Movie is:<br>$movie[0]<br>";
?>

<h2>It should get a movie by movie ID</h2>
<?php 
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$movie = MovieDB::getMoviesBy('movieID', 1);
echo "The value of Movie is:<br>$movie[0]<br>";
?>

<h2>It should get a movie by studio</h2>
<?php 
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$movie = MovieDB::getMoviesBy('studio', 'Universal Studios');
echo "The value of Movie is:<br>";
foreach($movie as $m){
	echo $m."<br>";
}
?>


</body>
</html>