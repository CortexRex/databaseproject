<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Basic tests for CopyDB</title>
</head>
<body>
<h1>CopyDB tests</h1>


<?php
include_once("../models/Database.class.php");
include_once("../models/Movie.class.php");
include_once("../models/MovieDB.class.php");
include_once("../models/Copy.class.php");
include_once("../models/CopyDB.class.php");
//include_once("./DBMaker.class.php");
?>


<h2>It should get all copies from a test database</h2>
<?php
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$copies = CopyDB::getCopiesBy();
$copyCount = count($copies);
echo "Number of copies in db is: $copyCount <br>";
foreach ($copies as $copy) 
	echo "$copy <br><br>";
?>	

<h2>It should allow a copy to be added</h2>
<?php  
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
echo "Number of copies in db before added is: ". count(CopyDB::getCopiesBy()) ."<br>";
$movie = MovieDB::getMoviesBy('title', 'The Dark Knight');
CopyDB::addCopies($movie[0],1);
echo "Number of copies in db after added is: ". count(CopyDB::getCopiesBy()) ."<br>";
?>

<h2>It should get copies by movieID</h2>
<?php 
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$copies = CopyDB::getCopiesBy('movieID', 1);
echo "The number of copies of movie 1 is:<br>".count($copies)."<br>";
?>


</body>
</html>