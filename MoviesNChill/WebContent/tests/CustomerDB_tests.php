<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Basic tests for CustomerDB</title>
</head>
<body>
<h1>CustomerDB tests</h1>


<?php
include_once("../models/Database.class.php");
include_once("../models/Customer.class.php");
include_once("../models/CustomerDB.class.php");
//include_once("./DBMaker.class.php");
?>


<h2>It should get all customers from a test database</h2>
<?php
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$customers= CustomerDB::getCustomersBy();
$customerCount = count($customers);
echo "Number of customers in db is: $customerCount <br>";
foreach ($customers as $customer) 
	echo "$customer <br><br>";
?>	

<h2>It should allow a customer to be added</h2>
<?php  
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
echo "Number of customers in db before added is: ". count(CustomerDB::getCustomersBy()) ."<br>";
$validTest = array("fName"=>"John", "lName"=> "Smith",  
		            "phone"=> "2104325432");
$customer = new Customer($validTest);
$newCustomer = CustomerDB::addCustomer(1,$customer);
echo "Number of users in db after added is: ". count(CustomerDB::getCustomersBy()) ."<br>";
echo "The new user is: $newCustomer<br>";
?>

<h2>It should get a Customer by lName</h2>
<?php 
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$customer = CustomerDB::getCustomersBy('lName', 'Smith');
echo "The value of customer Smith is:<br>$customer[0]<br>";
?>

<h2>It should get a Customer by phone</h2>
<?php 
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$customer = CustomerDB::getCustomersBy('phone', '2104325432');
echo "The value of customer is:<br>$customer[0]<br>";
?>

<h2>It should get a Customer by memberID</h2>
<?php 
Database::clearDB();
$db = Database::getDB('moviesnchilldb');
$customer = CustomerDB::getCustomersBy('memberID', '1');
echo "The value of customer is:<br>$customer[0]<br>";
?>

</body>
</html>