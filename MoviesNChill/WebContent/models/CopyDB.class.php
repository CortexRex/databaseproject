<?php
class CopyDB {
	

	public static function addCopies($movie, $count){
		$query = "CALL AddCopies(:movieID, :count)";
		
		try{
			if (is_null($movie) || $movie->getErrorCount() > 0)
				return $movie;
			$db = Database::getDB ();
			$statement = $db->prepare ($query);
			$statement->bindValue(":movieID",    $movie->getMovieID());
			$statement->bindValue(":count",      $count);
			$statement->execute ();
			$statement->closeCursor();
		}catch(Exception $e){
			
		}
	}
		
	public static function getCopyRowSetsBy($type = null, $value = null) {
		// Returns the rows of Users whose $type field has value $value
		$allowedTypes = ["copyID", "movieID"];
		$copyRowSets = array();
		try {
			$db = Database::getDB ();
			$query = "SELECT copyID, movieID FROM Copy";
			if (!is_null($type)) {
			    if (!in_array($type, $allowedTypes))
					throw new PDOException("$type not an allowed search criterion for Movie");
			    $query = $query. " WHERE ($type = :$type)";
			    $statement = $db->prepare($query);
			    $statement->bindParam(":$type", $value);
			} else 
				$statement = $db->prepare($query);
			$statement->execute ();
			$copyRowSets = $statement->fetchAll(PDO::FETCH_ASSOC);
			$statement->closeCursor ();
		} catch (Exception $e) { // Not permanent error handling
			echo "<p>Error getting copy rows by $type: " . $e->getMessage () . "</p>";
		}
		return $copyRowSets;
	}
	
	public static function getCopyArray($rowSets) {
		// Returns an array of User objects extracted from $rowSets
		$copies = array();
	 	if (!empty($rowSets)) {
			foreach ($rowSets as $copyRow ) {
				$copy = new Copy($copyRow);
				$copy->setCopyID($copyRow['copyID']);
				array_push ($copies, $copy );
			}
	 	}
		return $copies;
	}
	
	public static function getCopiesBy($type=null, $value=null) {
		// Returns Copy objects whose $type field has value $value
		$copyRows = CopyDB::getCopyRowSetsBy($type, $value);
		return CopyDB::getCopyArray($copyRows);
	}
	
	public static function getCopyValues($rowSets, $column) {
		// Returns an array of values from $column extracted from $rowSets
		$copyValues = array();
		foreach ($rowSets as $copyRow )  {
			$copyValue = $copyRow[$column];
			array_push ($copyValues, $copyValue);
		}
		return $copyValues;
	}
	
	public static function getCopyValuesBy($column, $type=null, $value=null) {
		// Returns the $column of copies whose $type field has value $value
		$copyRows = CopyDB::getCopyRowSetsBy($type, $value);
		return CopyDB::getCopyValues($copyRows, $column);
	}
	
	/*public static function updateUserData($userData) {
		// Update a user
		try {
			$db = Database::getDB ();
			if (is_null($userData) || $userData->getErrorCount() > 0)
				return $userData;
			$checkUserData = UserDataDB::getUserDatasBy('userId', $userData->getUserId());
			if (empty($checkUserData))
				$userData->setError('userId', 'USER_DOES_NOT_EXIST');
			if ($userData->getErrorCount() > 0)
				return $userData;
	
			$query = "UPDATE Users SET userName = :userName, password = :password, firstName = :firstName,
					  lastName = :lastName, gender = :gender, bday = :bday, email = :email, tel = :tel , pic = :pic
	    			                 WHERE userId = :userId";
	
			$statement = $db->prepare ($query);
			$statement->bindValue(":userName", $userData->getUserName());
			$statement->bindValue(":password", $userData->getPassword());
			$statement->bindValue(":userId", $userData->getUserId());
			$statement->bindValue(":firstName", $userData->getFirstName());
			$statement->bindValue(":lastName", $userData->getLastName());
			$statement->bindValue(":gender", $userData->getGender());
			$statement->bindValue(":bday", $userData->getBday());
			$statement->bindValue(":email", $userData->getEmail());
			$statement->bindValue(":tel", $userData->getTel());
			$statement->bindValue(":pic", $userData->getPic());
			$statement->execute ();
			$statement->closeCursor();
		} catch (Exception $e) { // Not permanent error handling
			$userData->setError('userId', 'USER_COULD_NOT_BE_UPDATED');
		}
		return $userData;
	} */
}
?>