<?php
include_once ("Messages.class.php");
class Payment {
	private $errorCount;
	private $errors;
	private $formInput;
	private $copyID;
	private $movieID;
	
    
	public function __construct($formInput = null) {
		$this->formInput = $formInput;
		Messages::reset();
		$this->initialize();
	}

	public function getError($errorName) {
		if (isset($this->errors[$errorName]))
			return $this->errors[$errorName];
		else
			return "";
	}

	public function setError($errorName, $errorValue) {
		// Sets a particular error value and increments error count
		if (!isset($this->errors, $errorName)) {
   		   $this->errors[$errorName] =  Messages::getError($errorValue);
		   $this->errorCount ++;
		}
	}
	
	public function setCopyID($id){
		$this->copyID = $id;
	}

	public function setMovieID($id){
		$this->movieID = $id;
	}
	
	public function getErrorCount() {
		return $this->errorCount;
	}

	public function getErrors() {
		return $this->errors;
	}


	public function getMovieID(){
		return $this->movieID;
	}
	
	public function getCopyID(){
		return $this->copyID;
	}
	
	public function getParameters() {
		// Return data fields as an associative array
		$paramArray = array("copyID"     => $this->copyID,
							"movieID"    => $this->movieID,
		);
		return $paramArray;
	}


	public function __toString() {
		$str = "<br>copyID: ".$this->copyID.
	           "<br>movieID: ".$this->movieID;

		return $str;
	}
	
	private function extractForm($valueName) {
		// Extract a stripped value from the form array
		$value = "";
		if (isset($this->formInput[$valueName])) {
			$value = trim($this->formInput[$valueName]);
			$value = stripslashes ($value);
			$value = htmlspecialchars ($value);
			return $value;
		}
	}
	
	private function initialize() {
		$this->errorCount = 0;
		$errors = array ();
		if (is_null ( $this->formInput ))
			$this->initializeEmpty();
		else {
			$this->validateCopyID();
			$this->validateMovieID();

		}
	}

	//need to initialize other forms!
	private function initializeEmpty() {
		$this->errorCount = 0;
		$errors = array();
		$this->copyID = "";
	 	$this->movieID = "";
	}

	private function validateCopyID(){
		$this->copyID = $this->extractForm('copyID');
	}
	private function validateMovieID(){
		$this->movieID = $this->extractForm('movieID');			
	}

}
?>