<?php
class MovieDB {
	
	
	public static function addMovie($movie){
		
		$query = "CALL AddMovie(:title, :rentalFee, :studio, @movieID)";
		
		try {
			if (is_null($movie) || $movie->getErrorCount() > 0)
				return $movie;
			$db = Database::getDB ();
			$statement = $db->prepare ($query);
			$statement->bindValue(":title",      $movie->getTitle());
			$statement->bindValue(":rentalFee",  $movie->getRentalFee());
			$statement->bindValue(":studio",       $movie->getStudio());
			$statement->execute ();
			$statement->closeCursor();
			$return = $db->query("SELECT @movieID AS movieID")->fetch(PDO::FETCH_ASSOC);
			$movie->setMovieID($return['movieID']);
		} catch (Exception $e) { // Not permanent error handling
			$movie->setError('movieID', 'MOVIE_INVALID');
		}
		return $movie;
		
	}
	
	public static function searchGenre($search){
	
		$query = "CALL SearchGenres(:search, @movieID, @title, @rentalFee, @studio)";
		try {
			if (is_null($search)){
				return $search;
			}
			$db = Database::getDB ();
			$statement = $db->prepare ($query);
			$statement->bindValue(":search", $search);
			$statement->execute ();
			$statement->closeCursor();
			$movieRowSets = $db->query("SELECT @movieID AS movieID, @title AS title, @rentalFee AS rentalFee, @studio AS studio")->fetchAll(PDO::FETCH_ASSOC);
			while ($row= $db->fetch_object()) {
				print_r($row);

			}
				
		} catch (Exception $e) { // Not permanent error handling

		}
		return MovieDB::getMovieArray($movieRowSets);
	}
	
	public static function addCopies($movie, $count){
		$query = "CALL AddCopies(:movieID, :count)";
		
		try{
			if (is_null($movie) || $movie->getErrorCount() > 0)
				return $movie;
			$db = Database::getDB ();
			$statement = $db->prepare ($query);
			$statement->bindValue(":movieID",    $movie->getMovieID());
			$statement->bindValue(":count",      $count);
			$statement->execute ();
			$statement->closeCursor();
		}catch(Exception $e){
			
		}
	}
	
	public static function addProducers($movie, $producers){
		foreach($producers as $producer){
			$query = "CALL AddProducer(:movieID, :producer)";
			try{
				if (is_null($producer) || is_null($movie))
					return $movie;
				$db = Database::getDB ();
				$statement = $db->prepare ($query);
				$statement->bindValue(":movieID",    $movie->getMovieID());
				$statement->bindValue(":producer",   $producer);
				$statement->execute ();
				$statement->closeCursor();
			}catch(Exception $e){
					
			}
		}
	}
		
	public static function addDirectors($movie, $directors){
		foreach($directors as $director){
			$query = "CALL AddDirector(:movieID, :director)";
			try{
				if (is_null($director) || is_null($movie))
					return $movie;
				$db = Database::getDB ();
				$statement = $db->prepare ($query);
				$statement->bindValue(":movieID",    $movie->getMovieID());
				$statement->bindValue(":director",   $director);
				$statement->execute ();
				$statement->closeCursor();
			}catch(Exception $e){
					
			}
		}
	}
	
	public static function addActors($movie, $actors){
		foreach($actors as $actor){
			$query = "CALL AddActor(:movieID, :actor)";
			try{
				if (is_null($actor) || is_null($movie))
					return $movie;
				$db = Database::getDB ();
				$statement = $db->prepare ($query);
				$statement->bindValue(":movieID",    $movie->getMovieID());
				$statement->bindValue(":actor",   $actor);
				$statement->execute ();
				$statement->closeCursor();
			}catch(Exception $e){
					
			}
		}
	}
	public static function getMovieRowSetsBy($type = null, $value = null) {
		// Returns the rows of Users whose $type field has value $value
		$allowedTypes = ["movieID", "title", "studio"];
		$movieRowSets = array();
		try {
			$db = Database::getDB ();
			$query = "SELECT movieID, title, rentalFee, studio FROM Movie";
			if (!is_null($type)) {
			    if (!in_array($type, $allowedTypes))
					throw new PDOException("$type not an allowed search criterion for Movie");
			    $query = $query. " WHERE ($type = :$type)";
			    $statement = $db->prepare($query);
			    $statement->bindParam(":$type", $value);
			} else 
				$statement = $db->prepare($query);
			$statement->execute ();
			$movieRowSets = $statement->fetchAll(PDO::FETCH_ASSOC);
			$statement->closeCursor ();
		} catch (Exception $e) { // Not permanent error handling
			echo "<p>Error getting movie rows by $type: " . $e->getMessage () . "</p>";
		}
		return $movieRowSets;
	}
	
	public static function getMovieActors($movieID){
		$actorRowSets = array();
		try{
			$db = Database::getDB ();
			$query = "SELECT actor FROM M_Actor";
			$query = $query. " WHERE (movieID = :$movieID)";
			$statement = $db->prepare($query);
			$statement->bindParam(":$movieID", $movieID);
			$statement->execute ();
			$actorRowSets = $statement->fetchAll(PDO::FETCH_ASSOC);
			$statement->closeCursor ();
		} catch (Exception $e){
			echo "<p>Error getting actor rows";	
		}
		$actors = array();
		if (!empty($actorRowSets)) {
			foreach ($actorRowSets as $actorRow ) {
				$actor= $actorRow['actor'];
				array_push ($actors, $actor );
			}
		}
		return $actors;
	}
	
	public static function getMovieDirectors($movieID){
		$directorRowSets = array();
		try{
			$db = Database::getDB ();
			$query = "SELECT director FROM M_Director";
			$query = $query. " WHERE (movieID = :$movieID)";
			$statement = $db->prepare($query);
			$statement->bindParam(":$movieID", $movieID);
			$statement->execute ();
			$directorRowSets = $statement->fetchAll(PDO::FETCH_ASSOC);
			$statement->closeCursor ();
		} catch (Exception $e){
			echo "<p>Error getting director rows";
		}
		$directors = array();
		if (!empty($directorRowSets)) {
			foreach ($directorRowSets as $directorRow ) {
				$director= $directorRow['director'];
				array_push ($directors, $director );
			}
		}
		return $directors;
	}
	
	public static function getMovieProducers($movieID){
		$producerRowSets = array();
		try{
			$db = Database::getDB ();
			$query = "SELECT producer FROM M_Producer";
			$query = $query. " WHERE (movieID = :$movieID)";
			$statement = $db->prepare($query);
			$statement->bindParam(":$movieID", $movieID);
			$statement->execute ();
			$producerRowSets = $statement->fetchAll(PDO::FETCH_ASSOC);
			$statement->closeCursor ();
		} catch (Exception $e){
			echo "<p>Error getting producer rows";
		}
		$producers = array();
		if (!empty($producerRowSets)) {
			foreach ($producerRowSets as $producerRow ) {
				$producer= $producerRow['producer'];
				array_push ($producers, $producer );
			}
		}
		return $producers;
	}
	
	public static function getMovieGenres($movieID){
		$genreRowSets = array();
		try{
			$db = Database::getDB ();
			$query = "SELECT genre FROM M_Genre";
			$query = $query. " WHERE (movieID = :$movieID)";
			$statement = $db->prepare($query);
			$statement->bindParam(":$movieID", $movieID);
			$statement->execute ();
			$genreRowSets = $statement->fetchAll(PDO::FETCH_ASSOC);
			$statement->closeCursor ();
		} catch (Exception $e){
			echo "<p>Error getting genre rows";
		}
		$genres = array();
		if (!empty($genreRowSets)) {
			foreach ($genreRowSets as $genreRow ) {
				$genre= $genreRow['genre'];
				array_push ($genres, $genre );
			}
		}
		return $genres;
	}
	
	
	public static function getMovieArray($rowSets) {
		// Returns an array of User objects extracted from $rowSets
		$movies = array();
	 	if (!empty($rowSets)) {
			foreach ($rowSets as $movieRow ) {
				$movie = new Movie($movieRow);
				$movie->setMovieID($movieRow['movieID']);
				array_push ($movies, $movie );
			}
	 	}
		return $movies;
	}
	
	public static function getMoviesBy($type=null, $value=null) {
		// Returns Movie objects whose $type field has value $value
		$movieRows = MovieDB::getMovieRowSetsBy($type, $value);
		return MovieDB::getMovieArray($movieRows);
	}
	
	public static function getMovieValues($rowSets, $column) {
		// Returns an array of values from $column extracted from $rowSets
		$movieValues = array();
		foreach ($rowSets as $movieRow )  {
			$movieValue = $movieRow[$column];
			array_push ($movieValues, $movieValue);
		}
		return $movieValues;
	}
	
	public static function getMovieValuesBy($column, $type=null, $value=null) {
		// Returns the $column of Employees whose $type field has value $value
		$movieRows = MovieDB::getMovieRowSetsBy($type, $value);
		return MovieDB::getMovieValues($movieRows, $column);
	}
	
	/*public static function updateUserData($userData) {
		// Update a user
		try {
			$db = Database::getDB ();
			if (is_null($userData) || $userData->getErrorCount() > 0)
				return $userData;
			$checkUserData = UserDataDB::getUserDatasBy('userId', $userData->getUserId());
			if (empty($checkUserData))
				$userData->setError('userId', 'USER_DOES_NOT_EXIST');
			if ($userData->getErrorCount() > 0)
				return $userData;
	
			$query = "UPDATE Users SET userName = :userName, password = :password, firstName = :firstName,
					  lastName = :lastName, gender = :gender, bday = :bday, email = :email, tel = :tel , pic = :pic
	    			                 WHERE userId = :userId";
	
			$statement = $db->prepare ($query);
			$statement->bindValue(":userName", $userData->getUserName());
			$statement->bindValue(":password", $userData->getPassword());
			$statement->bindValue(":userId", $userData->getUserId());
			$statement->bindValue(":firstName", $userData->getFirstName());
			$statement->bindValue(":lastName", $userData->getLastName());
			$statement->bindValue(":gender", $userData->getGender());
			$statement->bindValue(":bday", $userData->getBday());
			$statement->bindValue(":email", $userData->getEmail());
			$statement->bindValue(":tel", $userData->getTel());
			$statement->bindValue(":pic", $userData->getPic());
			$statement->execute ();
			$statement->closeCursor();
		} catch (Exception $e) { // Not permanent error handling
			$userData->setError('userId', 'USER_COULD_NOT_BE_UPDATED');
		}
		return $userData;
	} */
}
?>