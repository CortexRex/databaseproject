<?php
class CustomerDB {
	
	
	public static function addCustomer($employeeID,$customer){
		
		$query = "CALL AddCustomer(:employeeID, :fName, :lName, :phone, @memberID)";
		
		try {
			if (is_null($customer) || $customer->getErrorCount() > 0)
				return $customer;
			$db = Database::getDB ();
			$statement = $db->prepare ($query);
			$statement->bindValue(":employeeID", $employeeID);
			$statement->bindValue(":fName",      $customer->getFName());
			$statement->bindValue(":lName",      $customer->getLName());
			$statement->bindValue(":phone",      $customer->getPhone());
			$statement->execute ();
			$statement->closeCursor();
			$return = $db->query("SELECT @memberID AS memberID")->fetch(PDO::FETCH_ASSOC);
			$customer->setMemberID($return['memberID']);
		} catch (Exception $e) { // Not permanent error handling
			$customer->setError('memberID', 'MEMBER_INVALID');
		}
		return $customer;
	}
			
	public static function getCustomerRowSetsBy($type = null, $value = null) {
		// Returns the rows of Users whose $type field has value $value
		$allowedTypes = ["memberID", "lName", "phone"];
		$customerRowSets = array();
		try {
			$db = Database::getDB ();
			$query = "SELECT memberID, fName, lName, phone FROM Customer";
			if (!is_null($type)) {
			    if (!in_array($type, $allowedTypes))
					throw new PDOException("$type not an allowed search criterion for Users");
			    $query = $query. " WHERE ($type = :$type)";
			    $statement = $db->prepare($query);
			    $statement->bindParam(":$type", $value);
			} else 
				$statement = $db->prepare($query);
			$statement->execute ();
			$customerRowSets = $statement->fetchAll(PDO::FETCH_ASSOC);
			$statement->closeCursor ();
		} catch (Exception $e) { // Not permanent error handling
			echo "<p>Error getting customer rows by $type: " . $e->getMessage () . "</p>";
		}
		return $customerRowSets;
	}
	
	public static function getCustomerArray($rowSets) {
		// Returns an array of User objects extracted from $rowSets
		$customers = array();
	 	if (!empty($rowSets)) {
			foreach ($rowSets as $customerRow ) {
				$customer = new Customer($customerRow);
				$customer->setMemberID($customerRow['memberID']);
				array_push ($customers, $customer );
			}
	 	}
		return $customers;
	}
	
	public static function getCustomersBy($type=null, $value=null) {
		// Returns customer objects whose $type field has value $value
		$customerRows = CustomerDB::getCustomerRowSetsBy($type, $value);
		return CustomerDB::getCustomerArray($customerRows);
	}
	
	public static function getCustomerValues($rowSets, $column) {
		// Returns an array of values from $column extracted from $rowSets
		$customerValues = array();
		foreach ($rowSets as $customerRow )  {
			$customerValue = $customerRow[$column];
			array_push ($customerValues, $customerValue);
		}
		return $customerValues;
	}
	
	public static function getCustomerValuesBy($column, $type=null, $value=null) {
		// Returns the $column of Customers whose $type field has value $value
		$customerRows = CustomerDB::getCustomerRowSetsBy($type, $value);
		return CustomerDB::getCustomerValues($customerRows, $column);
	}
	
	/*public static function updateUserData($userData) {
		// Update a user
		try {
			$db = Database::getDB ();
			if (is_null($userData) || $userData->getErrorCount() > 0)
				return $userData;
			$checkUserData = UserDataDB::getUserDatasBy('userId', $userData->getUserId());
			if (empty($checkUserData))
				$userData->setError('userId', 'USER_DOES_NOT_EXIST');
			if ($userData->getErrorCount() > 0)
				return $userData;
	
			$query = "UPDATE Users SET userName = :userName, password = :password, firstName = :firstName,
					  lastName = :lastName, gender = :gender, bday = :bday, email = :email, tel = :tel , pic = :pic
	    			                 WHERE userId = :userId";
	
			$statement = $db->prepare ($query);
			$statement->bindValue(":userName", $userData->getUserName());
			$statement->bindValue(":password", $userData->getPassword());
			$statement->bindValue(":userId", $userData->getUserId());
			$statement->bindValue(":firstName", $userData->getFirstName());
			$statement->bindValue(":lastName", $userData->getLastName());
			$statement->bindValue(":gender", $userData->getGender());
			$statement->bindValue(":bday", $userData->getBday());
			$statement->bindValue(":email", $userData->getEmail());
			$statement->bindValue(":tel", $userData->getTel());
			$statement->bindValue(":pic", $userData->getPic());
			$statement->execute ();
			$statement->closeCursor();
		} catch (Exception $e) { // Not permanent error handling
			$userData->setError('userId', 'USER_COULD_NOT_BE_UPDATED');
		}
		return $userData;
	} */
}
?>