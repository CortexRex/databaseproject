<?php
include_once ("Messages.class.php");
class Customer {
	private $errorCount;
	private $errors;
	private $formInput;
	private $memberID;
	private $fName;
	private $lName;
    private $phone;

    
	public function __construct($formInput = null) {
		$this->formInput = $formInput;
		Messages::reset();
		$this->initialize();
	}

	public function getError($errorName) {
		if (isset($this->errors[$errorName]))
			return $this->errors[$errorName];
		else
			return "";
	}

	public function setError($errorName, $errorValue) {
		// Sets a particular error value and increments error count
		if (!isset($this->errors, $errorName)) {
   		   $this->errors[$errorName] =  Messages::getError($errorValue);
		   $this->errorCount ++;
		}
	}
	
	public function setMemberID($id){
		$this->memberID = $id;
	}

	public function getErrorCount() {
		return $this->errorCount;
	}

	public function getErrors() {
		return $this->errors;
	}

	public function getMemberID(){
		return $this->memberID;
	}
	
	public function getFName() {
		return $this->fName;
	}
	
	public function getLName() {
		return $this->lName;
	}

	public function getPhone() {
		return $this->phone;
	}
	
	public function getParameters() {
		// Return data fields as an associative array
		$paramArray = array("fName"    => $this->fName,
				            "lName"    => $this->lName,
							"phone" => $this->phone
		);
		return $paramArray;
	}


	public function __toString() {
		$str = "<br>First name: ".$this->fName.
		       "<br>Last name: ".$this->lName.
		       "<br>phone: ".$this->phone;
		return $str;
	}
	
	private function extractForm($valueName) {
		// Extract a stripped value from the form array
		$value = "";
		if (isset($this->formInput[$valueName])) {
			$value = trim($this->formInput[$valueName]);
			$value = stripslashes ($value);
			$value = htmlspecialchars ($value);
			return $value;
		}
	}
	
	private function initialize() {
		$this->errorCount = 0;
		$errors = array ();
		if (is_null ( $this->formInput ))
			$this->initializeEmpty();
		else {
			$this->validateFName();
			$this->validateLName();
            $this->validatePhone();
		}
	}

	//need to initialize other forms!
	private function initializeEmpty() {
		$this->errorCount = 0;
		$errors = array();
	 	$this->fName = "";
	 	$this->lName = "";
	 	$this->phone = "";
	}

	
	private function validateFName() {
		// First name should only contain letters
		$this->fName = $this->extractForm('fName');
		if (empty($this->fName))
			$this->setError('fName', 'FIRST_NAME_EMPTY');
		elseif (!filter_var($this->fName, FILTER_VALIDATE_REGEXP,
			array("options"=>array("regexp" =>"/^([a-zA-Z])+$/i")) )) {
			$this->setError('fName', 'FIRST_NAME_HAS_INVALID_CHARS');
		}
	}
	
	private function validateLName() {
		// Last name should only contain letters, blanks, hyphens and '
		$this->lName = $this->extractForm('lName');
		if (empty($this->lName)) 
			$this->setError('lName', 'LAST_NAME_EMPTY');
		elseif (!filter_var($this->lName, FILTER_VALIDATE_REGEXP, //todo
				array("options"=>array("regexp" =>"/^([a-zA-Z])+$/i")) )) {
			$this->setError('lName', 'LAST_NAME_HAS_INVALID_CHARS');
		}
	}
	

	private function validatePhone(){
		$this->phone = $this->extractForm('phone');
		if(empty($this->phone)) {
			$this->setError('phone', 'TEL_EMPTY');
			return;
		}
		$this->phone = preg_replace("/[^0-9]/",'', $this->phone);
		if(strlen($this->phone) == 11)
			$this->phone = preg_replace("/^1/",'', $this->phone);
		if(strlen($this->phone) != 10)
			$this->setError('phone', 'TEL_INVALID');
	}
		
}
?>