<?php
include_once ("Messages.class.php");
class Movie {
	private $errorCount;
	private $errors;
	private $formInput;
	private $movieID;
	private $title;
	private $rentalFee;
	private $studio;
	
    
	public function __construct($formInput = null) {
		$this->formInput = $formInput;
		Messages::reset();
		$this->initialize();
	}

	public function getError($errorName) {
		if (isset($this->errors[$errorName]))
			return $this->errors[$errorName];
		else
			return "";
	}

	public function setError($errorName, $errorValue) {
		// Sets a particular error value and increments error count
		if (!isset($this->errors, $errorName)) {
   		   $this->errors[$errorName] =  Messages::getError($errorValue);
		   $this->errorCount ++;
		}
	}
	
	public function setMovieID($id){
		$this->movieID = $id;
	}

	public function getErrorCount() {
		return $this->errorCount;
	}

	public function getErrors() {
		return $this->errors;
	}


	public function getMovieID(){
		return $this->movieID;
	}
	
	public function getTitle() {
		return $this->title;
	}
	
	public function getRentalFee() {
		return $this->rentalFee;
	}

	public function getStudio() {
		return $this->studio;
	}
	

	public function getParameters() {
		// Return data fields as an associative array
		$paramArray = array("title"     => $this->title,
							"rentalFee"    => $this->rentalFee,
							"studio"     => $this->studio
		);
		return $paramArray;
	}


	public function __toString() {
		$str = "<br>title: ".$this->title.
	           "<br>rentalFee: ".$this->rentalFee.
		       "<br>studio: ".$this->studio;

		return $str;
	}
	
	private function extractForm($valueName) {
		// Extract a stripped value from the form array
		$value = "";
		if (isset($this->formInput[$valueName])) {
			$value = trim($this->formInput[$valueName]);
			$value = stripslashes ($value);
			$value = htmlspecialchars ($value);
			return $value;
		}
	}
	
	private function initialize() {
		$this->errorCount = 0;
		$errors = array ();
		if (is_null ( $this->formInput ))
			$this->initializeEmpty();
		else {
			$this->validateTitle();
			$this->validateRentalFee();
			$this->validateStudio();
		}
	}

	//need to initialize other forms!
	private function initializeEmpty() {
		$this->errorCount = 0;
		$errors = array();
		$this->title = "";
	 	$this->rentalFee = "";
	 	$this->studio = "";
	}

	private function validateTitle(){
		$this->title = $this->extractForm('title');
	}
	private function validateRentalFee(){
		$this->rentalFee = $this->extractForm('rentalFee');			
	}

	private function validateStudio(){
		$this->studio = $this->extractForm('studio');
	}
}
?>