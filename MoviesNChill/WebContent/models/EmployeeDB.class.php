<?php
class EmployeeDB {
	
	
	public static function addEmployee($adminID,$employee){
		
		$query = "CALL AddEmployee(:adminID, :role, :login, :hash, :fName,
		          :lName, :phone, :locationID, @employeeID)";
		
		try {
			if (is_null($employee) || $employee->getErrorCount() > 0)
				return $employee;
			$db = Database::getDB ();
			$statement = $db->prepare ($query);
			$statement->bindValue(":adminID", $adminID);
			$statement->bindValue(":role",       $employee->getRole());
			$statement->bindValue(":login",      $employee->getLogin());
			$statement->bindValue(":hash",       $employee->getHash());
			$statement->bindValue(":fName",      $employee->getFName());
			$statement->bindValue(":lName",      $employee->getLName());
			$statement->bindValue(":phone",      $employee->getPhone());
			$statement->bindValue(":locationID", $employee->getLocationID());
			$statement->execute ();
			$statement->closeCursor();
			$return = $db->query("SELECT @employeeID AS employeeID")->fetch(PDO::FETCH_ASSOC);
			$employee->setEmployeeId($return['employeeID']);
		} catch (Exception $e) { // Not permanent error handling
			$employee->setError('employeeID', 'USER_INVALID');
		}
		return $employee;
		
	}
	
	public static function removeEmployee($adminID, $employeeID){
		
		$query = "Call RemoveEmployee(:adminID, :employeeID)";
		
		try{
			$db = Database::getDB();
			$statement = $db->prepare($query);
			$statement->bindValue(":adminID", $adminID);
			$statement->bindValue(":employeeID", $employeeID);
			$statement->execute();
			$statement->closeCursor();
		} catch (Exception $e){
			return false;
		}
		return true;
	}
	

	public static function getEmployeeRowSetsBy($type = null, $value = null) {
		// Returns the rows of Users whose $type field has value $value
		$allowedTypes = ["employeeId", "login"];
		$employeeRowSets = array();
		try {
			$db = Database::getDB ();
			$query = "SELECT employeeID, role, login, hash, fName, lName, hireDate, phone FROM employee";
			if (!is_null($type)) {
			    if (!in_array($type, $allowedTypes))
					throw new PDOException("$type not an allowed search criterion for Users");
			    $query = $query. " WHERE ($type = :$type)";
			    $statement = $db->prepare($query);
			    $statement->bindParam(":$type", $value);
			} else 
				$statement = $db->prepare($query);
			$statement->execute ();
			$employeeRowSets = $statement->fetchAll(PDO::FETCH_ASSOC);
			$statement->closeCursor ();
		} catch (Exception $e) { // Not permanent error handling
			echo "<p>Error getting employee rows by $type: " . $e->getMessage () . "</p>";
		}
		return $employeeRowSets;
	}
	
	public static function getEmployeeArray($rowSets) {
		// Returns an array of User objects extracted from $rowSets
		$employees = array();
	 	if (!empty($rowSets)) {
			foreach ($rowSets as $employeeRow ) {
				$employee = new Employee($employeeRow);
				$employee->setEmployeeId($employeeRow['employeeID']);
				array_push ($employees, $employee );
			}
	 	}
		return $employees;
	}
	
	public static function getEmployeesBy($type=null, $value=null) {
		// Returns Employee objects whose $type field has value $value
		$employeeRows = EmployeeDB::getEmployeeRowSetsBy($type, $value);
		return EmployeeDB::getEmployeeArray($employeeRows);
	}
	
	public static function getEmployeeValues($rowSets, $column) {
		// Returns an array of values from $column extracted from $rowSets
		$employeeValues = array();
		foreach ($rowSets as $employeeRow )  {
			$employeeValue = $employeeRow[$column];
			array_push ($employeeValues, $employeeValue);
		}
		return $employeeValues;
	}
	
	public static function getEmployeeValuesBy($column, $type=null, $value=null) {
		// Returns the $column of Employees whose $type field has value $value
		$employeeRows = EmployeeDB::getEmployeeRowSetsBy($type, $value);
		return EmployeeDB::getEmployeeValues($employeeRows, $column);
	}
	
	/*public static function updateUserData($userData) {
		// Update a user
		try {
			$db = Database::getDB ();
			if (is_null($userData) || $userData->getErrorCount() > 0)
				return $userData;
			$checkUserData = UserDataDB::getUserDatasBy('userId', $userData->getUserId());
			if (empty($checkUserData))
				$userData->setError('userId', 'USER_DOES_NOT_EXIST');
			if ($userData->getErrorCount() > 0)
				return $userData;
	
			$query = "UPDATE Users SET userName = :userName, password = :password, firstName = :firstName,
					  lastName = :lastName, gender = :gender, bday = :bday, email = :email, tel = :tel , pic = :pic
	    			                 WHERE userId = :userId";
	
			$statement = $db->prepare ($query);
			$statement->bindValue(":userName", $userData->getUserName());
			$statement->bindValue(":password", $userData->getPassword());
			$statement->bindValue(":userId", $userData->getUserId());
			$statement->bindValue(":firstName", $userData->getFirstName());
			$statement->bindValue(":lastName", $userData->getLastName());
			$statement->bindValue(":gender", $userData->getGender());
			$statement->bindValue(":bday", $userData->getBday());
			$statement->bindValue(":email", $userData->getEmail());
			$statement->bindValue(":tel", $userData->getTel());
			$statement->bindValue(":pic", $userData->getPic());
			$statement->execute ();
			$statement->closeCursor();
		} catch (Exception $e) { // Not permanent error handling
			$userData->setError('userId', 'USER_COULD_NOT_BE_UPDATED');
		}
		return $userData;
	} */
}
?>