<?php
include_once ("Messages.class.php");
class Employee {
	private $errorCount;
	private $errors;
	private $formInput;
	private $employeeId;
	private $role;
	private $login;
	private $hash;
	private $fName;
	private $lName;
    private $hireDate;
    private $phone;
    private $password;
    private $locationID;
    
	public function __construct($formInput = null) {
		$this->formInput = $formInput;
		Messages::reset();
		$this->initialize();
	}

	public function getError($errorName) {
		if (isset($this->errors[$errorName]))
			return $this->errors[$errorName];
		else
			return "";
	}

	public function setError($errorName, $errorValue) {
		// Sets a particular error value and increments error count
		if (!isset($this->errors, $errorName)) {
   		   $this->errors[$errorName] =  Messages::getError($errorValue);
		   $this->errorCount ++;
		}
	}
	
	public function setEmployeeId($id){
		$this->employeeId = $id;
	}

	public function getErrorCount() {
		return $this->errorCount;
	}

	public function getErrors() {
		return $this->errors;
	}


	public function getEmployeeId(){
		return $this->employeeId;
	}
	
	public function getFName() {
		return $this->fName;
	}
	
	public function getLName() {
		return $this->lName;
	}

	public function getRole() {
		return $this->role;
	}
	
	public function getLogin() {
		return $this->login;
	}
	
	public function getPhone() {
		return $this->phone;
	}
	
	public function getHash() {
		return $this->hash;
	}
	
	public function getHireDate() {
		return $this->hireDate;
	}
	
	public function getLocationID(){
		return $this->locationID;
	}
	
	public function getParameters() {
		// Return data fields as an associative array
		$paramArray = array("role"     => $this->role,
							"login"    => $this->login,
							"hash"     => $this->hash,
							"fName"    => $this->fName,
				            "lName"    => $this->lName,
							"hireDate" => $this->hireDate,
							"phone" => $this->phone,
							"locationID => $this->locationID"
		);
		return $paramArray;
	}


	public function __toString() {
		$str = "<br>role: ".$this->role.
	           "<br>user login: ".$this->login.
		       "<br>hash: ".$this->hash.
			   "<br>First name: ".$this->fName.
		       "<br>Last name: ".$this->lName.
		       "<br>phone: ".$this->phone.
				"<br>hire date: ".$this->hireDate.
				"<br>locationID: ".$this->locationID;

		return $str;
	}
	
	private function extractForm($valueName) {
		// Extract a stripped value from the form array
		$value = "";
		if (isset($this->formInput[$valueName])) {
			$value = trim($this->formInput[$valueName]);
			$value = stripslashes ($value);
			$value = htmlspecialchars ($value);
			return $value;
		}
	}
	
	private function initialize() {
		$this->errorCount = 0;
		$errors = array ();
		if (is_null ( $this->formInput ))
			$this->initializeEmpty();
		else {
			$this->validateRole();
			$this->validateFName();
			$this->validateLName();
            $this->validatePhone();
            $this->validateLogin();
            $this->validateHash();
            $this->validatePassword();
            $this->validateHireDate();
            $this->validateLocationID();
		}
	}

	//need to initialize other forms!
	private function initializeEmpty() {
		$this->errorCount = 0;
		$errors = array();
		$this->role = "";
	 	$this->fName = "";
	 	$this->lName = "";
	 	$this->phone = "";
	 	$this->login = "";
	 	$this->password = "";
	 	$this->hash = "";
	 	$this->hireDate = "";
	 	$this->locationID = "";
	}

	private function validateLocationID(){
		$this->locationID = $this->extractForm('locationID');
	}
	private function validateRole(){
		$this->role = $this->extractForm('role');	
		
	}
	
	private function validateHash(){
		if(isset($this->formInput['hash'])){
	    	$this->hash = $this->extractForm('hash');
		}
	}
	
	private function validateHireDate(){
		$this->hireDate = $this->extractForm('hireDate');
	}
	
	private function validateFName() {
		// First name should only contain letters
		$this->fName = $this->extractForm('fName');
		if (empty($this->fName))
			$this->setError('fName', 'FIRST_NAME_EMPTY');
		elseif (!filter_var($this->fName, FILTER_VALIDATE_REGEXP,
			array("options"=>array("regexp" =>"/^([a-zA-Z])+$/i")) )) {
			$this->setError('fName', 'FIRST_NAME_HAS_INVALID_CHARS');
		}
	}
	
	private function validateLName() {
		// Last name should only contain letters, blanks, hyphens and '
		$this->lName = $this->extractForm('lName');
		if (empty($this->lName)) 
			$this->setError('lName', 'LAST_NAME_EMPTY');
		elseif (!filter_var($this->lName, FILTER_VALIDATE_REGEXP, //todo
				array("options"=>array("regexp" =>"/^([a-zA-Z])+$/i")) )) {
			$this->setError('lName', 'LAST_NAME_HAS_INVALID_CHARS');
		}
	}
	

	private function validatePhone(){
		$this->phone = $this->extractForm('phone');
		if(empty($this->phone)) {
			$this->setError('phone', 'TEL_EMPTY');
			return;
		}
		$this->phone = preg_replace("/[^0-9]/",'', $this->phone);
		if(strlen($this->phone) == 11)
			$this->phone = preg_replace("/^1/",'', $this->phone);
		if(strlen($this->phone) != 10)
			$this->setError('phone', 'TEL_INVALID');
	}
	
	private function validateLogin() {
		//Username should only contain letters, numbers, dashes and underscore
		$this->login = $this->extractForm('login');
		if (empty($this->login))
			$this->setError('login', 'USER_NAME_EMPTY');
		elseif (!filter_var($this->login, FILTER_VALIDATE_REGEXP,
				array("options"=>array("regexp" =>"/^([a-zA-Z0-9\-\_])+$/i")) ))
				$this->setError('login', 'USER_NAME_HAS_INVALID_CHARS');
	}
	
	private function validatePassword() {
		if(!$this->hash){
		
			$this->password = $this->extractForm('password');
			if (empty($this->password)){
				$this->setError('password', 'PASSWORD_EMPTY');
				$this->password = null;
			}
			elseif (!filter_var($this->password, FILTER_VALIDATE_REGEXP,
					array("options"=>array("regexp" =>"/^([a-zA-Z0-9\-\_])+$/i")) )){
					$this->setError('password', 'PASSWORD_HAS_INVALID_CHARS');
					$this->password = null;
			}	
			else{
				$this->hash = hash('sha512', $this->password);
				$this->password = null;
			}
		}
	}	
	
}
?>